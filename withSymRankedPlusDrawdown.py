#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import codecs
import re
import os
import datetime
import time
import numpy
import scipy
import pickle
import collections
import operator
from datetime import timedelta, date, datetime
from decimal import Decimal
from scipy import stats, optimize
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot,dates



import math
from math import sqrt


patcomma = re.compile(r",", re.DOTALL)
patcolon = re.compile(r":", re.DOTALL)
patdot = re.compile(r"\.",re.DOTALL)
patNonIsoDateN225 = re.compile(r"(\d\d\d\d)(\d\d)(\d\d)",re.DOTALL)
patNonIsoDateDow = re.compile(r"(\d\d)\/(\d\d)\/(\d\d)",re.DOTALL)
patLast2Digit = re.compile(r"\d\d(\d\d)",re.DOTALL)


with open('bugSym.pickle', mode='rb') as handle:
    bugSym = pickle.load(handle)



ccounter = 0




debugFile =open("debug","w")




delta = timedelta(days=1)





def makeSymList(symFile):
    latestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2016/y161230.txt", "rb")
    earliestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2000/y000104.txt", "rb")
    symList = []
    latestList = []
    earliestList = []
    symLines = symFile.readlines()
#    print symLines
    latestLines = latestPriceFile.readlines()
    earliestLines = earliestPriceFile.readlines()
    for l in latestLines:
        sym = l.split("\t")[0].strip()
        latestList.append(sym)
#    print latestList
    for e in earliestLines:
        sym = e.split("\t")[0].strip()
#        print "sym in earliest"
#        print sym
        earliestList.append(sym)
#    print earliestList
    for s in symLines:
        sym = s.strip()
        symList.append(sym)
    return(set(symList) & set(latestList) & set(earliestList))


def makeLotSizeDict(symList):
    lotsizefile = open("/home/wailoktam/tradeData/stock.csv", "r")
    lotsizedict = {}
    lotsizelines = lotsizefile.readlines()
    for lotsizeline in lotsizelines:
        lotsizes = patcomma.split(lotsizeline)
        sym = lotsizes[0].strip()
        if sym in symList:
            lotsizedict[sym] = lotsizes[6].strip()
    lotsizefile.close()
    return(lotsizedict)        

def tick_cal(price):
    if price < 2000:
        tick = 1
    elif price >= 2000 and price < 3000:
        tick = 5
    elif price >= 3000 and price < 30000:
        tick = 10
    elif price >= 30000 and price < 50000:
        tick = 50
    elif price >= 50000 and price < 100000:
        tick = 100
    elif price >= 100000 and price < 1000000:
        tick = 1000
    elif price >= 1000000 and price < 20000000:
        tick = 10000
    elif price >= 20000000 and price < 30000000:
        tick = 50000
    else:
        tick = 100000
    return(tick)

def date_generator(start_date, end_date, delta):
    d = start_date
    while d <= end_date:
        yield d
        d += delta

def neg_date_generator(start_date, end_date, delta):
    d = start_date
    while d >= end_date:
        yield d
        d -= delta

def convertDateFormat4N225(dateStr):
    matchDateStr = patNonIsoDateN225.search(dateStr)
    isoStr = matchDateStr.group(1)+"-"+matchDateStr.group(2)+"-"+matchDateStr.group(3)
    return (isoStr)

def convertDateFormat4Dow(dateStr):
    matchDateStr = patNonIsoDateDow.search(dateStr)
    isoStr = "20"+matchDateStr.group(3)+"-"+matchDateStr.group(1)+"-"+matchDateStr.group(2)
    return (isoStr)


def n225DictFunct(n225FileList):
    n225Dict = {}
    for fN in n225FileList:
        f = open(fN,"rb")
        n225Lines = f.readlines()
        for nL in n225Lines:
            n = patcomma.split(nL)
            dayStr = n[0].strip()
            n225Dict[convertDateFormat4N225(dayStr)] = n[4].strip()
    return(n225Dict)

def entry_cal(prevHike, yori, hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow,dowPrevPrevHike):
    prevDowPlusOrMinusSpec = float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow))/2
    prevDowChange = (float(dowPrevHike) - float(dowPrevPrevHike)) / float(dowPrevPrevHike)
#    prevDowChange = (float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow))/2)/((float(dowPrevHigh) + float(dowPrevLow))/2)
#    debugFile.write("prevDowPlusOrMinus\n")
#    debugFile.write(str(prevDowPlusOrMinusSpec)+"\n")
#    debugFile.write("dowPrevPrevHike*1.02\n")
##    debugFile.write(str(float(dowPrevPrevHike)*1.002)+"\n")
#    debugFile.write("dowPrevHike in entry_cal\n")
#    debugFile.write(str(float(dowPrevHike))+"\n")
    if prevDowPlusOrMinusSpec > 0 and float(dowPrevHike) > (float(dowPrevPrevHike)*1.005) :
        entryPrice = float(prevHike) * (1+ prevDowChange)
    else:
        entryPrice = 0
    return(entryPrice)

def exit_cal(entryPrice):
#    exitPrice = entryPrice * 1.01
    exitPrice = 0
    return(exitPrice)


#def entry_cal(price,commod,dow,formula,vhsd,y,position):
#    if price < 2000: new_price = round(price)
#    elif price >= 2000 and price < 3000:  new_price = round(price/5)*5
#    elif price >= 3000 and price < 30000: new_price = round(price,-1)
#    elif price >= 30000 and price < 50000: new_price = round(price/5,-1)*5
#    elif price >= 50000 and price < 100000: new_price = round(price,-2)
#    elif price >= 100000 and price < 1000000: new_price = round(price,-3)
#    elif price >= 1000000 and price < 20000000: new_price = round(price,-4)
#    elif price >= 20000000 and price < 30000000: new_price = round(price/5,-4)*5
#    else: new_price = round(price,-5)
#    if position == "l":
#        while 1:
#            new_price = new_price - tick_cal(new_price)
#            if new_price <= price + formula[0][2]+string.atof(commod)*formula[0][0]+string.atof(dow)*formula[0][1]- y * vhsd:
#                break
#    elif position == "s":
#        while 1:
##            new_price = new_price + tick_cal(new_price)
#            if new_price >= price + formula[0][2]+string.atof(commod)*formula[0][0]+string.atof(dow)*formula[0][1]+ y * vhsd:
#                break
#    return(new_price)

#def exit_cal(price,commod,dow,formula,vhsd,y,position):
#    if price < 2000: new_price = round(price)
#    elif price >= 2000 and price < 3000:  new_price = round(price/5)*5
#    elif price >= 3000 and price < 30000: new_price = round(price,-1)
#    elif price >= 30000 and price < 50000: new_price = round(price/5,-1)*5
#    elif price >= 50000 and price < 100000: new_price = round(price,-2)
#    elif price >= 100000 and price < 1000000: new_price = round(price,-3)
#    elif price >= 1000000 and price < 20000000: new_price = round(price,-4)
#    elif price >= 20000000 and price < 30000000: new_price = round(price/5,-4)*5
#    else: new_price = round(price,-5)
#    if position == "l":
#        while 1:
#            new_price = new_price + tick_cal(new_price)
#            if new_price >= price + formula[0][2]+string.atof(commod)*formula[0][0]+string.atof(dow)*formula[0][1]+ y * vhsd:
#                break
#    elif position == "s":
#        while 1:
#            new_price = new_price - tick_cal(new_price)
#            if new_price <= price + formula[0][2]+string.atof(commod)*formula[0][0]+string.atof(dow)*formula[0][1]- y * vhsd:
#                break
#    return(new_price)



def corr_cal(commoddict_list,carry_dict_list):
    stocklist = []
    pstocklist = []
    daylist = []
    resultlist = []
    storelist = []
    commodlistoflist = []
    stop = 0
    dcounter = 0
    checkfile = open("../Python/ship_check.csv","w")
    for meigara in carry_dict_list:
        stocklist.append([])
        pstocklist.append([])
        daylist.append([])
    ccounter = 0
    for commoddict in commoddict_list:
        commodlistoflist.append([])
        storelist.append(0)
    for currentday in neg_date_generator(date(2008, 12, 25), date(2008, 1, 1), timedelta(days=1)):
        mcounter = 0
        if carry_dict_list[mcounter].has_key(currentday.isoformat()):
            while 1:
#                print mcounter
                stocklist[mcounter].append(carry_dict_list[mcounter][currentday.isoformat()])
                daylist[mcounter].append(currentday.isoformat())
                mcounter=mcounter + 1
                if mcounter > (len(carry_dict_list)-1): break
            yesterday = currentday - delta
            while 1:
                flag = 1
               
                for commoddict in commoddict_list:
                    if not commoddict.has_key(yesterday.isoformat()):
                        flag = 0
                if flag == 1:
                    ccounter = 0
                    for commoddict in commoddict_list:
                        storelist[ccounter] = storelist[ccounter] + string.atof(commoddict[yesterday.isoformat()])
                        ccounter = ccounter + 1
                    if carry_dict_list[0].has_key(yesterday.isoformat()):
                        ccounter = 0
                        for commoddict in commoddict_list:
                            commodlistoflist[ccounter].append(storelist[ccounter])
                            storelist[ccounter] = 0
                            ccounter = ccounter + 1
                        break
                else:
                    mcounter = 0
                    if carry_dict_list[mcounter].has_key(yesterday.isoformat()):
                        while 1:
                            stocklist[mcounter].pop()
                            daylist[mcounter].pop()
                            mcounter=mcounter + 1
                            if mcounter > (len(carry_dict_list)-1): break
                        break
                if yesterday >= datetime.datetime.fromtimestamp(time.mktime(time.strptime("2008-01-1","%Y-%m-%d"))).date():
                    yesterday  = yesterday - delta
                else:
                    ccounter = 0
                    for commoddict in commoddict_list:
                        commodlistoflist[ccounter].append(storelist[ccounter])
                        storelist[ccounter] = 0
                        ccounter = ccounter + 1
                    break

#    print [len(daylist[0]),len(stocklist[0]),len(commodlistoflist[0]),len(commodlistoflist[1])]
    for day in daylist[0]:
#        print [stocklist[0][dcounter],commodlistoflist[0][dcounter],commodlistoflist[1][dcounter]]
        checkfile.write(day+","+str((stocklist[0])[dcounter])+","+str((commodlistoflist[0])[dcounter])+","+str((commodlistoflist[1])[dcounter])+"\n")
#        checkfile.write(day+","+((stocklist[0])[dcounter])+","+((commodlistoflist[0])[dcounter])+","+((commodlistoflist[1])[dcounter])+"\n")
        dcounter=dcounter+1
    

    
    commod1array = scipy.array(commodlistoflist[0])
    commod2array = scipy.array(commodlistoflist[1])
    formula_list = []
    for i in range(len(carry_dict_list)):
        formula_list.append(numpy.linalg.lstsq(numpy.column_stack([commod1array, commod2array,numpy.ones(len(commod1array),float)]),scipy.array(stocklist[i])))
        for j in range(len(commod1array)):
            pstocklist[i].append(formula_list[i][0][2]+commod1array[j]*formula_list[i][0][0]+commod2array[j]*formula_list[i][0][1])
        print scipy.stats.pearsonr(scipy.array(stocklist[i]),scipy.array(pstocklist[i]))
        

    checkfile.close()
    return(formula_list)

def vhsd_cal(meigaralist):
    #standard deviation
    vhsd_list = []
    for meigara in meigaralist:
        vhsd_sum = 0
        vcounter = 0
        for currentday in date_generator(date(2008, 1, 1), date(2008, 12, 23), timedelta(days=1)):
            temp=[]
            pricelist = []
            sumdiff = 0
            oldhour = "08"
            for shou in range(1,5):
                if shou <= 3: shou_cp = shou
                else: shou_cp = 9
                for bu in range(1,5):
                    shoubu = str(shou_cp) + str(bu)
                    day2="".join(currentday.isoformat().split("-")) 
                    candlefilename = "../candles/"+day2+"/"+meigara+"."+shoubu+".csv"
                    try:
                        candlefile = open(candlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(candlefilename)>0 :
                            rows=candlefile.readlines()
                            for row in rows:
                                cells=patcomma.split(row)
                                temp.append(tuple(cells))
                                if patcolon.split(cells[0])[0] != oldhour: pricelist.append(string.atoi(cells[4]))
                                oldhour = patcolon.split(cells[0])[0]
                            candlefile.close()
            temp=sort(temp,0)
            try:
                hike = string.atoi(temp[len(temp)-1][4])
                for price in pricelist:
                    sumdiff = sumdiff + (price - hike)**2
#                   print currentday,(price - hike)**2
                vhsd_sum = vhsd_sum + sqrt(sumdiff)/len(pricelist)
#                print currentday,len(pricelist),sumdiff,sqrt(sumdiff)/len(pricelist)

                vcounter = vcounter + 1
            except IndexError:
                pass
        vhsd_list.append(vhsd_sum/vcounter)
    return(vhsd_list)    



def carry_dicts(intraday_dict,meigaralist):
    carry_dict_list = []
    for meigara in meigaralist:
        unfinished_dict = {}
        for currentday in date_generator(date(2008, 1, 5), date(2008, 12, 23), timedelta(days=1)):
            yesterday = currentday - delta
            while 1:
                if intraday_dict.has_key(yesterday.isoformat()):
                    break
                else:
                    yesterday = yesterday - delta
            temp=[]
            for shou in range(1,5):
                if shou <= 3: shou_cp = shou
                else: shou_cp = 9
                for bu in range(1,5):
                    shoubu = str(shou_cp) + str(bu)
                    day2="".join(currentday.isoformat().split("-"))
                    candlefilename = "../candles/"+day2+"/"+meigara+"."+shoubu+".csv"
                    try:
                        candlefile = open(candlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(candlefilename)>0 :
                            rows=candlefile.readlines()
                            for row in rows:
                                cells=patcomma.split(row)
                                temp.append(tuple(cells))
                        candlefile.close()
            temp=sort(temp,0)
            try:
                prevtemp=[]
                for shou in range(1,5):
                    if shou <= 3: shou_cp = shou
                    else: shou_cp = 9
                    for bu in range(1,5):
                        shoubu = str(shou_cp) + str(bu)
                        day3="".join(yesterday.isoformat().split("-")) 
                        prevcandlefilename = "../candles/"+day3+"/"+meigara+"."+shoubu+".csv"
#                     print prevcandlefilename
                        try:
                            prevcandlefile = open(prevcandlefilename, "r")
                        except IOError:
                            pass
                        else:
                            if os.path.getsize(prevcandlefilename)>0 :
                                rows=prevcandlefile.readlines()
                                for row in rows:
                                    cells=patcomma.split(row)
                                    prevtemp.append(tuple(cells))
                            prevcandlefile.close()       
                prevtemp=sort(prevtemp,0)
#            print prevtemp
                prevhike = (string.atoi(prevtemp[len(prevtemp)-1][4]))
#            yori = (string.atoi(temp[0][1]))*(string.atoi(subdict[meigara]))
                hike = (string.atoi(temp[len(temp)-1][4]))
#            min_entry = entry_cal(prevhike,dow_percent*Decimal(beta_dict[meigara]))
                carry = hike - prevhike 
                unfinished_dict[currentday.isoformat()] = carry
            except IndexError:
#            print meigara,prevcandlefilename
                pass
        carry_dict_list.append(unfinished_dict)
    return(carry_dict_list)



def intraday_dicts(meigaralist):
    intraday_dict_list = []
    for meigara in meigaralist:
        unfinished_dict = {}
        for currentday in date_generator(date(2008, 1, 1), date(2008, 12, 23), timedelta(days=1)):
            temp=[]
            for shou in range(1,5):
                if shou <= 3: shou_cp = shou
                else: shou_cp = 9
                for bu in range(1,5):
                    shoubu = str(shou_cp) + str(bu)
                    day2="".join(currentday.isoformat().split("-"))
                    candlefilename = "../candles/"+day2+"/"+meigara+"."+shoubu+".csv"
                    try:
                        candlefile = open(candlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(candlefilename)>0 :
                            rows=candlefile.readlines()
                            for row in rows:
                                cells=patcomma.split(row)
                                temp.append(tuple(cells))
                        candlefile.close()
            temp=sort(temp,0)
            try:
                yori = (string.atof(temp[0][1]))
                hike = (string.atof(temp[len(temp)-1][4]))
                intraday = hike-yori
                unfinished_dict[currentday.isoformat()] = intraday
                candlefile.close()
            except IndexError:
                pass
        intraday_dict_list.append(unfinished_dict)
    return(intraday_dict_list)




def sort(list, field):
    res = []                                     # always returns a list
    for x in list:
        i = 0
        for y in res:
            if x[field] <= y[field]: break       # list node goes here?
            i = i+1
        res[i:i] = [x]                           # insert in result slot
    return res

def yori_b_lcts_plus(pick,meigara,b_ri):
#def yori_b_hike_s(day1,meigara,ri):
    global b_katsu,b_make,b_wake
    temp=[]
    for shou in range(1,5):
        if shou <= 3: shou_cp = shou
        else: shou_cp = 9
        for bu in range(1,5):
            shoubu = str(shou_cp) + str(bu)
            day2="".join(pick[0].split("-"))
#            day2="".join(day1.split("-"))
            candlefilename = "C:/Downloads/candles/"+day2+"/"+meigara+"."+shoubu+".csv"
            try:
                candlefile = open(candlefilename, "r")
            except IOError:
                pass
            else:
                if os.path.getsize(candlefilename)>0 :
                    rows=candlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        temp.append(tuple(cells))
    temp=sort(temp,0)
    try:
        yori = (string.atoi(temp[0][1]))*1000
        lc = round((yori*0.98),-3)
        rcounter = 0
        peak = yori
        safe = yori
#        found = 0
        while 1:
            if (string.atoi(temp[rcounter][2])*1000 >= (yori*1.02)) and (string.atoi(temp[rcounter][2])*1000 > peak):
                    peak = string.atoi(temp[rcounter][2])*1000
            if (string.atoi(temp[rcounter][2])*1000 >= (yori*1.01)): 
                    safe = yori*1.01
            if (string.atoi(temp[rcounter][3])*1000 <= lc) and (string.atoi(temp[rcounter][2])*1000 > lc):
                tejimai = lc
                action = "lc"
                break
            elif (string.atoi(temp[rcounter][3])*1000 < lc) and (string.atoi(temp[rcounter][2])*1000 <= lc):
                tejimai = string.atoi(temp[rcounter][2])*1000
                action = "lc"
                break
            else:
                if (safe != yori) and (string.atoi(temp[rcounter][3])*1000<=yori) and (string.atoi(temp[rcounter][2])*1000>yori):
                    tejimai = yori
                    action = "safe"
                    break
                elif (safe != yori) and (string.atoi(temp[rcounter][3])*1000<yori) and (string.atoi(temp[rcounter][2])*1000<=yori):
                    tejimai = string.atoi(temp[rcounter][2])*1000
                    action = "safe"
                    break
                if (peak != yori) and (string.atoi(temp[rcounter][3])*1000<=(yori+((peak-yori)*0.5))) and (string.atoi(temp[rcounter][2])*1000>((yori+(peak-yori)*0.5))):
                    tejimai = round((yori+((peak-yori)*0.5)),-3)
                    action = "ts"
                    break
                elif (peak != yori) and (string.atoi(temp[rcounter][3])*1000<(yori+((peak-yori)*0.5))) and (string.atoi(temp[rcounter][2])*1000<=(yori+((peak-yori)*0.5))):
                    tejimai = round(string.atoi(temp[rcounter][2])*1000,-3)
                    action = "ts"
                    break
            if rcounter == len(temp)-1:
                tejimai = string.atoi(temp[len(temp)-1][4])*1000
                action = "hk"
                break
            rcounter = rcounter + 1 
        b_ri = b_ri+(tejimai-yori)
#        print(pick[0],yori,tejimai,peak,action,rcounter,peak-tejimai,tejimai-yori)
        print(pick[0],yori,tejimai,tejimai-yori,peak,action)
#    print(pick[0],yori,hike,yori/string.atof(pick[1]),hike/string.atof(pick[1]))

        if tejimai-yori>0 : b_katsu=b_katsu+1
        elif tejimai-yori<0 : b_make=b_make+1
        else:b_wake=b_wake+1
        candlefile.close()
    except IndexError:
        pass
    return(b_ri)

def yori_s_lcts_plus(pick,meigara,s_ri):
    global s_katsu,s_make,s_wake
    temp=[]
    for shou in range(1,5):
        if shou <= 3: shou_cp = shou
        else: shou_cp = 9
        for bu in range(1,5):
            shoubu = str(shou_cp) + str(bu)
            day2="".join(pick[0].split("-"))
            candlefilename = "C:/Downloads/candles/"+day2+"/"+meigara+"."+shoubu+".csv"
            try:
                candlefile = open(candlefilename, "r")
            except IOError:
                pass
            else:
                if os.path.getsize(candlefilename)>0 :
                    rows=candlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        temp.append(tuple(cells))
    temp=sort(temp,0)
    try:
        yori = (string.atoi(temp[0][1]))*1000
        lc = round((yori*1.02),-3)
        rcounter = 0
        peak = yori
        safe = yori
#        found = 0
        while 1:
            if (string.atoi(temp[rcounter][3])*1000 <= (yori*0.98)) and (string.atoi(temp[rcounter][3])*1000 < peak):
                    peak = string.atoi(temp[rcounter][3])*1000
            if (string.atoi(temp[rcounter][3])*1000 <= (yori*0.99)): 
                    safe = yori*0.99
            if (string.atoi(temp[rcounter][2])*1000 >= lc) and (string.atoi(temp[rcounter][3])*1000 < lc):
                tejimai = lc
                action = "lc"
                break
            elif (string.atoi(temp[rcounter][2])*1000 > lc) and (string.atoi(temp[rcounter][3])*1000 >= lc):
                tejimai = string.atoi(temp[rcounter][3])*1000
                action = "lc"
                break
            else:
                if (safe != yori) and (string.atoi(temp[rcounter][2])*1000>=yori) and (string.atoi(temp[rcounter][3])*1000<yori):
                    tejimai = yori
                    action = "safe"
                    break
                elif (safe != yori) and (string.atoi(temp[rcounter][2])*1000>yori) and (string.atoi(temp[rcounter][3])*1000>=yori):
                    tejimai = string.atoi(temp[rcounter][3])*1000
                    action = "safe"
                    break
                if (peak != yori) and (string.atoi(temp[rcounter][2])*1000>=(yori-((yori-peak)*0.5))) and (string.atoi(temp[rcounter][3])*1000<(yori-((yori-peak)*0.5))):
                    tejimai = round(yori-((yori-peak)*0.5),-3)
                    action = "ts"
                    break
                elif (peak != yori) and (string.atoi(temp[rcounter][2])*1000>(yori-((peak-yori)*0.5))) and (string.atoi(temp[rcounter][3])*1000>=(yori-((peak-yori)*0.5))):
                    tejimai = round(string.atoi(temp[rcounter][3])*1000,-3)
                    action = "ts"
                    break
            if rcounter == len(temp)-1:
                tejimai = string.atoi(temp[len(temp)-1][4])*1000
                action = "hk"
                break
            rcounter = rcounter + 1 
        s_ri = s_ri+(yori-tejimai)
#        print(pick[0],yori,tejimai,peak,action,rcounter,peak-tejimai,tejimai-yori)
        print(pick[0],yori,tejimai,yori-tejimai,peak,action)
#    print(pick[0],yori,hike,yori/string.atof(pick[1]),hike/string.atof(pick[1]))

        if yori-tejimai>0 : s_katsu=s_katsu+1
        elif yori-tejimai<0 : s_make=s_make+1
        else:s_wake=s_wake+1
        candlefile.close()
    except IndexError:
        pass
    return(s_ri)

def yori_b_lcts(pick,meigara,ri):
#def yori_b_hike_s(day1,meigara,ri):
    global katsu,make,wake
    temp=[]
    for shou in range(1,5):
        if shou <= 3: shou_cp = shou
        else: shou_cp = 9
        for bu in range(1,5):
            shoubu = str(shou_cp) + str(bu)
            day2="".join(pick[0].split("-"))
#            day2="".join(day1.split("-"))
            candlefilename = "C:/Downloads/candles/"+day2+"/"+meigara+"."+shoubu+".csv"
            try:
                candlefile = open(candlefilename, "r")
            except IOError:
                pass
            else:
                if os.path.getsize(candlefilename)>0 :
                    rows=candlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        temp.append(tuple(cells))
    temp=sort(temp,0)
    try:
        yori = (string.atoi(temp[0][1]))*1000
        lc = round((yori*0.98),-3)
        rcounter = 0
        peak = yori
#        found = 0
        while 1:
            if (string.atoi(temp[rcounter][2])*1000 >= (yori*1.02)) and (string.atoi(temp[rcounter][2])*1000 > peak):
                    peak = string.atoi(temp[rcounter][2])*1000
            if (string.atoi(temp[rcounter][3])*1000 <= lc) and (string.atoi(temp[rcounter][2])*1000 > lc):
                tejimai = lc
                action = "lc"
                break
            elif (string.atoi(temp[rcounter][3])*1000 < lc) and (string.atoi(temp[rcounter][2])*1000 <= lc):
                tejimai = string.atoi(temp[rcounter][2])*1000
                action = "lc"
                break
            else:
                if (peak != yori) and (string.atoi(temp[rcounter][3])*1000<=(yori+((peak-yori)*0.5))) and (string.atoi(temp[rcounter][2])*1000>(yori+((peak-yori)*0.5))):
                    tejimai = round(yori+((peak-yori)*0.5),-3)
                    action = "ts"
                    break
                elif (peak != yori) and (string.atoi(temp[rcounter][3])*1000<(yori+((peak-yori)*0.5))) and (string.atoi(temp[rcounter][2])*1000<=(yori+((peak-yori)*0.5))):
                    tejimai = round(string.atoi(temp[rcounter][2])*1000,-3)
                    action = "ts"
                    break
            if rcounter == len(temp)-1:
                tejimai = string.atoi(temp[len(temp)-1][4])*1000
                action = "hk"
                break
            rcounter = rcounter + 1 
        ri = ri+(tejimai-yori)
#        print(pick[0],yori,tejimai,peak,action,rcounter,peak-tejimai,tejimai-yori)
        print(pick[0],yori,tejimai,tejimai-yori,action)
#    print(pick[0],yori,hike,yori/string.atof(pick[1]),hike/string.atof(pick[1]))

        if tejimai-yori>0 : katsu=katsu+1
        elif tejimai-yori<0 : make=make+1
        else:wake=wake+1
        candlefile.close()
    except IndexError:
        pass
    return(ri)

def yori_b_lc(pick,meigara,b_ri):
#def yori_b_hike_s(day1,meigara,ri):
    global b_katsu,b_make,b_wake
    temp=[]
    prev=[]
    for shou in range(1,5):
        if shou <= 3: shou_cp = shou
        else: shou_cp = 9
        for bu in range(1,5):
            shoubu = str(shou_cp) + str(bu)
            day2="".join(pick[0].split("-"))
            day1obj=datetime.datetime.fromtimestamp(time.mktime(time.strptime(pick[0],"%Y-%m-%d")))-timedelta(days=1)
            while 1:
                    day1="".join(day1obj.date().isoformat().split("-"))
                    candlefolder = "C:/Downloads/candles/"+day1
                    if os.path.exists(candlefolder): break
                    day1obj= day1obj - timedelta(days=1)
            candlefilename = "C:/Downloads/candles/"+day2+"/"+meigara+"."+shoubu+".csv"
            prvcandlefilename = "C:/Downloads/candles/"+day1+"/"+meigara+"."+shoubu+".csv"
            try:
                candlefile = open(candlefilename, "r")
                prvcandlefile = open(prvcandlefilename, "r")
            except IOError:
                pass
            else:
                if os.path.getsize(candlefilename)>0 :
                    rows=candlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        temp.append(tuple(cells))
                if os.path.getsize(prvcandlefilename)>0 :
                    rows=prvcandlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        prev.append(tuple(cells))
    temp=sort(temp,0)
    prev=sort(prev,0)
    try:
        lastclose = (string.atoi(prev[len(prev)-1][4]))*1000
        yori = (string.atoi(temp[0][1]))*1000
        gap = (yori - lastclose)*1.0
        lc = round((yori*0.97),-3)
        rcounter = 0
        while 1:
            if (string.atof(pick[1])-(gap/lastclose) <= 0.02):
                tejimai = yori
                action = "mo"
                break
            if (string.atoi(temp[rcounter][3])*1000 <= lc) and (string.atoi(temp[rcounter][2])*1000 > lc):
                tejimai = lc
                action = "lc"
                break
            elif (string.atoi(temp[rcounter][3])*1000 < lc) and (string.atoi(temp[rcounter][2])*1000 <= lc):
                tejimai = string.atoi(temp[rcounter][2])*1000
                action = "lc"
                break
            if rcounter == len(temp)-1:
                tejimai = string.atoi(temp[len(temp)-1][4])*1000
                action = "hk"
                break
            rcounter = rcounter + 1 
        b_ri = b_ri+(tejimai-yori)
#        if pick[0]=="2007-08-15": print(temp)
        print(pick[0],yori,tejimai,action,gap/lastclose,gap,tejimai-yori)
#    print(pick[0],yori,hike,yori/string.atof(pick[1]),hike/string.atof(pick[1]))
        if tejimai-yori>0 : b_katsu=b_katsu+1
        elif tejimai-yori<0 : b_make=b_make+1
        else:b_wake=b_wake+1
        candlefile.close()
    except IndexError:
        pass
    return(b_ri)

def yori_s_lc(pick,meigara,s_ri):
#def yori_b_hike_s(day1,meigara,ri):
    global s_katsu,s_make,s_wake
    temp=[]
    prev=[]
    for shou in range(1,5):
        if shou <= 3: shou_cp = shou
        else: shou_cp = 9
        for bu in range(1,5):
            shoubu = str(shou_cp) + str(bu)
            day2="".join(pick[0].split("-"))
            day1obj=datetime.datetime.fromtimestamp(time.mktime(time.strptime(pick[0],"%Y-%m-%d")))-timedelta(days=1)
            while 1:
                    day1="".join(day1obj.date().isoformat().split("-"))
                    candlefolder = "C:/Downloads/candles/"+day1
                    if os.path.exists(candlefolder): break
                    day1obj= day1obj - timedelta(days=1)
            candlefilename = "C:/Downloads/candles/"+day2+"/"+meigara+"."+shoubu+".csv"
            prvcandlefilename = "C:/Downloads/candles/"+day1+"/"+meigara+"."+shoubu+".csv"
            try:
                candlefile = open(candlefilename, "r")
                prvcandlefile = open(prvcandlefilename, "r")
            except IOError:
                pass
            else:
                if os.path.getsize(candlefilename)>0 :
                    rows=candlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        temp.append(tuple(cells))
                if os.path.getsize(prvcandlefilename)>0 :
                    rows=prvcandlefile.readlines()
                    for row in rows:
                        cells=patcomma.split(row)
                        prev.append(tuple(cells))
    temp=sort(temp,0)
    prev=sort(prev,0)
    try:
        lastclose = (string.atoi(prev[len(prev)-1][4]))*1000
        yori = (string.atoi(temp[0][1]))*1000
        gap = (yori - lastclose)*1.0
        lc = round((yori*1.03),-3)
        rcounter = 0
        while 1:
            if (string.atof(pick[1])-(gap/lastclose) >= -0.02):
                tejimai = yori
                action = "mo"
                break
            if (string.atoi(temp[rcounter][2])*1000 >= lc) and (string.atoi(temp[rcounter][3])*1000 < lc):
                tejimai = lc
                action = "lc"
                break
            elif (string.atoi(temp[rcounter][2])*1000 > lc) and (string.atoi(temp[rcounter][3])*1000 >= lc):
                tejimai = string.atoi(temp[rcounter][3])*1000
                action = "lc"
                break 
            if rcounter == len(temp)-1:
                tejimai = string.atoi(temp[len(temp)-1][4])*1000
                action = "hk"
                break
            rcounter = rcounter + 1 
        s_ri = s_ri+(yori-tejimai)
#        if pick[0]=="2007-08-15": print(temp)
        print(pick[0],yori,tejimai,action,gap/lastclose,gap,yori-tejimai)
#    print(pick[0],yori,hike,yori/string.atof(pick[1]),hike/string.atof(pick[1]))
        if yori-tejimai>0 : s_katsu=s_katsu+1
        elif yori-tejimai<0 : s_make=s_make+1
        else:s_wake=s_wake+1
        candlefile.close()
    except IndexError:
        pass
    return(s_ri)


def makeSymPriceDict(symList,fullPath):
    symOpens = {}
    symCloses = {}
    symHighs = {}
    symLows = {}
    file = open(fullPath,"rb")
    for l in file.readlines():
        if l.split("\t")[0] in symList:
            symOpens[l.split("\t")[0]] = float(l.split("\t")[2])
            symCloses[l.split("\t")[0]] = float(l.split("\t")[5])
            symHighs[l.split("\t")[0]] = float(l.split("\t")[3])
            symLows[l.split("\t")[0]] = float(l.split("\t")[4])
    file.close()
    return (symOpens,symCloses,symHighs,symLows)



def int_s_hike_b(isoDate,symList,profs,prevIsoDate,dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow,dowPrevPrevHike,budget, bugSym, katsu, make, wake):

    daikin = 0
    mcounter = 0

    debugFile.write(isoDate+"\n")

    lotSizeDict= makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2]+".txt"
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2]+".txt"
    prevFullPath = prevFolderPath+"/"+prevFilePath
    fullPath = folderPath+"/"+filePath
    try:
        symOpens, symCloses, symHighs, _ = makeSymPriceDict(symList,fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        _,prevSymCloses,_,_ = makeSymPriceDict(symList,prevFullPath)
        if stockPriceFileFound ==1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    entry = 0
#    print "stockPriceFound"
#    print stockPriceFileFound
    if stockPriceFileFound == 1:
 #       for s in ["4704"]:
        for s in symList:

#            try:
#            if s in max20ShortProfs1stYr:
            if s not in bugSym:
                print "prevSymCloses"
                print prevSymCloses
                prevHike = prevSymCloses[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]

                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow,dowPrevPrevHike)
#        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

                print "symbol"
                print s
                print "yori"
                print yori
                print "hike"
                print hike
                print "high"
                print high
                print "prevHike"
                print prevHike
                print "minEntry"
                print minEntryPrice
                print "entry"
                print entry
                debugFile.write("dowPrevyori\n")
                debugFile.write(str(dowPrevYori)+"\n")
                debugFile.write("dowPrevhike\n")
                debugFile.write(str(dowPrevHike)+"\n")
                debugFile.write("dowPrevhigh\n")
                debugFile.write(str(dowPrevHigh)+"\n")
                debugFile.write("dowPrevLow\n")
                debugFile.write(str(dowPrevLow)+"\n")
                debugFile.write("dowPrevprevHike\n")
                debugFile.write(str(dowPrevPrevHike)+"\n")



                if entry ==  1:

                    maxExitPrice = exit_cal(entryPrice)

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike


                    if not profs.get(s,0) == 0:
                        profs[s] = profs[s]+(entryPrice-tejimai)*float(lotSizeDict[s])*(round(budget/(entryPrice*float(lotSizeDict[s]))-0.5))
                    else:
                        profs[s] = (entryPrice - tejimai) * float(lotSizeDict[s]) * (round(budget / (entryPrice * float(lotSizeDict[s])) - 0.5))
#            daikin = daikin + entry
                    if entryPrice-tejimai>0 : katsu=katsu+1
                    elif entryPrice-tejimai<0 : make=make+1
                    else: wake=wake+1


#            except KeyError:
#                bugSym[s] = ""
#                pass
    return(profs, katsu, make, wake)





def int_b_hike_s(day1,meigaralist,b_ri,prev,commod,dow,formula_list,vhsd_list,y):
    global b_katsu,b_make,b_wake
    daikin = 0
    mcounter = 0
#    subdict=lotsizedict()
    for meigara in meigaralist:
        temp=[]
        for shou in range(1,5):
             if shou <= 3: shou_cp = shou
             else: shou_cp = 9
             for bu in range(1,5):
                 shoubu = str(shou_cp) + str(bu)
                 day2="".join(day1.split("-")) 
                 candlefilename = "../candles/"+day2+"/"+meigara+"."+shoubu+".csv"
                 try:
                     candlefile = open(candlefilename, "r")
                 except IOError:
                     pass
                 else:
                     if os.path.getsize(candlefilename)>0 :
                         rows=candlefile.readlines()
                         for row in rows:
                             cells=patcomma.split(row)
                             temp.append(tuple(cells))
                     candlefile.close()
        temp=sort(temp,0)
        try:
            prevtemp=[]
            for shou in range(1,5):
                 if shou <= 3: shou_cp = shou
                 else: shou_cp = 9
                 for bu in range(1,5):
                     shoubu = str(shou_cp) + str(bu)
                     day3="".join(prev.split("-")) 
                     prevcandlefilename = "../candles/"+day3+"/"+meigara+"."+shoubu+".csv"
                     try:
                         prevcandlefile = open(prevcandlefilename, "r")
                     except IOError:
                         pass
                     else:
                         if os.path.getsize(prevcandlefilename)>0 :
                             rows=prevcandlefile.readlines()
                             for row in rows:
                                 cells=patcomma.split(row)
                                 prevtemp.append(tuple(cells))
                         prevcandlefile.close()       
            prevtemp=sort(prevtemp,0)
            prevhike = (string.atoi(prevtemp[len(prevtemp)-1][4]))
            yori = (string.atoi(temp[0][1]))*(string.atoi(subdict[meigara]))
            hike = (string.atoi(temp[len(temp)-1][4]))*(string.atoi(subdict[meigara]))

            max_entry = entry_cal(prevhike,commod,dow,formula_list[mcounter],vhsd_list[mcounter],y,"l")*(string.atoi(subdict[meigara]))
            min_exit = exit_cal(prevhike,commod,dow,formula_list[mcounter],vhsd_list[mcounter],y,"l")*(string.atoi(subdict[meigara]))
            
            if yori <= max_entry :
                entry = yori
                action = "entry"
                rcounter = 0
                while 1:
                    if (string.atoi(temp[rcounter][1])*(string.atoi(subdict[meigara])) <= min_exit) and (string.atoi(temp[rcounter][2])*(string.atoi(subdict[meigara])) > min_exit):
                        tejimai = min_exit
                        break
                    elif (string.atoi(temp[rcounter][1])*(string.atoi(subdict[meigara])) > min_exit):
                        tejimai = string.atoi(temp[rcounter][1])*(string.atoi(subdict[meigara]))
                        break
                    else:
                        if rcounter == len(temp)-1:
                            tejimai = string.atoi(temp[len(temp)-1][4])*(string.atoi(subdict[meigara]))
                            break
                    rcounter = rcounter + 1 
            else:
                rcounter = 0
                action = "noentry"
                while 1:
                    if (string.atoi(temp[rcounter][1])* (string.atoi(subdict[meigara]))< max_entry):
                        entry = string.atoi(temp[rcounter][1])* (string.atoi(subdict[meigara]))
                        action = "entry"
                        entry_pt = rcounter
                        break
                    elif (string.atoi(temp[rcounter][1])* (string.atoi(subdict[meigara]))>= max_entry) and (string.atoi(temp[rcounter][2])* (string.atoi(subdict[meigara]))< max_entry):
                        entry = max_entry
                        action = "entry"
                        entry_pt = rcounter
                        break
                    if rcounter == len(temp)-1:
                        break
                    rcounter = rcounter + 1
                rcounter = 0
                if action == "entry":
                    while 1:
                        if (string.atoi(temp[rcounter][1])*(string.atoi(subdict[meigara])) <= min_exit) and (string.atoi(temp[rcounter][2])*(string.atoi(subdict[meigara])) > min_exit) and rcounter > entry_pt:
                            tejimai = min_exit
                            break
                        elif (string.atoi(temp[rcounter][1])*(string.atoi(subdict[meigara])) > min_exit) and rcounter > entry_pt:
                            tejimai = string.atoi(temp[rcounter][1])*(string.atoi(subdict[meigara]))
                            break
                        else:
                            if rcounter == len(temp)-1:
                                tejimai = string.atoi(temp[len(temp)-1][4])*(string.atoi(subdict[meigara]))
                                break
                        rcounter = rcounter + 1 
 
            if action != "noentry":
                b_ri = b_ri+(tejimai-entry)
                daikin = daikin + entry
                if tejimai-entry>0 : b_katsu=b_katsu+1
                elif tejimai - entry<0 : b_make=b_make+1
                else:b_wake=b_wake+1
#                print(day1,meigara,entry,tejimai,entry-tejimai,min_entry)
            candlefile.close()
            prevcandlefile.close()
        except IndexError:
            pass
        mcounter = mcounter + 1
    return((b_ri,daikin))    




def tesuuryou(daikin):
    if daikin == 0:
        charge = 0
    elif daikin <= 500000:
        charge = 315
    elif daikin <= 1000000:
        charge = 840
    elif daikin <= 2000000:
        charge = 1680
    elif daikin <= 3000000:
        charge = 2520
    elif daikin <= 4000000:
        charge = 3360
    elif daikin <= 5000000:
        charge = 4200
    elif daikin <= 6000000:
        charge = 5040
    elif daikin <= 7000000:
        charge = 5880
    return(charge)


def makeDowDicts(file):
    dowOpens = {}
    dowCloses = {}
    dowHighs = {}
    dowLows = {}
    for dL in reversed(file.readlines()):
        dCells = patcomma.split(dL.strip())
        dowOpens[convertDateFormat4Dow(dCells[0])] = dCells[1]
        dowCloses[convertDateFormat4Dow(dCells[0])] = dCells[4]
        dowHighs[convertDateFormat4Dow(dCells[0])] = dCells[2]
        dowLows[convertDateFormat4Dow(dCells[0])] = dCells[3]
#    print dowDict
    return(dowOpens,dowCloses,dowHighs,dowLows)


def plotPL(plDict,figFileName):



#    for d in plDict.keys():
#        print "content of plDict"
#        print d
#        print "after apply strptime"
#       e = (datetime.strptime(d, "%Y-%m-%d"))
#        print e
#        print "after apply date2num"
#        print matplotlib.dates.date2num(e)

    x = matplotlib.dates.date2num([ datetime.strptime(d, "%Y-%m-%d") for d in sorted(plDict)])
    y = [plDict[d] for d in sorted(plDict)]
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    ax.plot(x, y)
    years      = matplotlib.dates.YearLocator()
    yearsFmt = matplotlib.dates.DateFormatter('%Y')
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(yearsFmt)
    fig.autofmt_xdate()
    pyplot.savefig(figFileName)


def yearLooper(year,profs4All,realProfs, max20ShortProfsLastYr, symList, dowOpens, dowCloses,dowHighs, dowLows,n225Dict,budget,bugSym):
    global allKatsu, allMake, allWake, selKatsu, selMake, selWake
    global monthlyPL
    global annualPL
    monthlyPL  = {}
    annualPL = {}
    lossFlag = 0
    conscLoss = 0
    maxDrawDn = 0
    accuLoss = 0
    accuGain = 0


#    year = 2001
#    if year == 2004:
    while year <= 2016:
#        if year == 2000:
#            firstTradeDay = 5
#        else:
        firstTradeDay = 1
        for currentday in date_generator(date(year, 1, firstTradeDay), date(year, 12, 31), timedelta(days=1)):

        #        print currentday.isoformat()
            if n225Dict.has_key(currentday.isoformat()):
                yesterday225 = currentday - delta
                yesterday = currentday - delta

                print yesterday
                print yesterday225
                while 1:
                    if dowOpens.has_key(yesterday.isoformat()) and dowOpens.has_key(yesterday.isoformat()):
                        break
                    else:
                    #                   print yesterday
                        yesterday = yesterday - delta
                while 1:
                    if n225Dict.has_key(yesterday225.isoformat()):
                        break
                    else:
                    #                    print yesterday225
                        yesterday225 = yesterday225 - delta

                dayB4Yest = yesterday - delta

                while 1:
                    if dowOpens.has_key(dayB4Yest.isoformat()):
                        break
                    else:
                    #                    print yesterday225
                        dayB4Yest = dayB4Yest - delta

                    #            old_b_ri = b_ri
                    #            if string.atof(patcomma.split(filterdict[yesterday.isoformat()])[1]) < -0.002:
                    #                (b_ri,daikin)=int_b_hike_s(currentday.isoformat(),all,s_ri,yesterday225.isoformat(),commoddict[yesterday.isoformat()],dowdict[yesterday.isoformat()],formula_list,vhsd_list,x[0])
                    #                b_ri = b_ri - tesuuryou(daikin)
                    #                if b_ri-old_b_ri>0:
                    #                    b_katsuhi=b_katsuhi+1
                    #                elif b_ri-old_b_ri<0:
                    #                    b_makehi=b_makehi+1

                    #           old_s_ri = s_ri
                    #           print old_s_ri
                    #           if string.atof(patcomma.split(filterdict[yesterday.isoformat()])[1]) > 0.002:

                if not currentday.month == yesterday225.month:
                    monthlyPL[yesterday225.isoformat()] = sum(realProfs.values())
#                    monthlyPL[yesterday225.isoformat()] = sum(profs4All.values())

                if not currentday.year == yesterday225.year:
                    annualPL[yesterday225.isoformat()] = sum(realProfs.values())
#                    annualPL[yesterday225.isoformat()] = sum(profs4All.values())

                dPrevHike = dowCloses[yesterday.isoformat()]
                dPrevYori = dowOpens[yesterday.isoformat()]
                dPrevHigh = dowHighs[yesterday.isoformat()]
                dPrevLow = dowLows[yesterday.isoformat()]
                dPrevPrevHike = dowCloses[dayB4Yest.isoformat()]
                oldTotalProfSel = sum(realProfs.values())
                profs4All, allKatsu, allMake, allWake= int_s_hike_b(currentday.isoformat(), symList, profs4All, yesterday225.isoformat(), dPrevYori,
                                      dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike, budget, bugSym, allKatsu, allMake, allWake)
                realProfs, selKatsu, selMake, selWake = int_s_hike_b(currentday.isoformat(), list(max20ShortProfsLastYr.keys()), realProfs, yesterday225.isoformat(), dPrevYori,
                                      dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike, budget, bugSym, selKatsu, selMake, selWake)
                loss = sum(realProfs.values()) - oldTotalProfSel


                if loss >= 0:
                    lossFlag = 1
                    accuLoss += loss
                else:
                    lossFlag = 0
                    accuGain += -(loss)

                if lossFlag == 0:
                    conscLoss = 0

                if lossFlag == 1:
                    conscLoss  += loss

                if conscLoss >= maxDrawDn:
                    maxDrawDn = accuLoss
#                max20ShortProfsLastYr = dict(sorted(profs4All.iteritems(), key=operator.itemgetter(1), reverse=True)[:20])
#                print "max10"
#                print max10ShortProfsLastYr
#        print str(year)+":"str(-(loss))

        year += 1
    pf = accuGain/accuLoss
    return profs4All, realProfs, maxDrawDn, pf


if __name__ == "__main__":
# ship(x, done=0):
    budget = 5000000
    startYear = 2010
    realProfs = {}

    global allKatsu, allMake, allWake, selKatsu, selMake, selWake, monthlyPL, annualPL
    allKatsu=0
    allMake=0
    allWake=0
    selKatsu=0
    selMake=0
    selWake=0
#    tCount = 0
#    filterdict=filterbydowdict()
#    shipfile = open("../market/baltic.csv","r")
    board1SymListFile =  open("/home/wailoktam/tradeData/tosho1.csv", "rb")
    symList = makeSymList(board1SymListFile)
    with open('max20ShortProfs1Yr.pickle', 'rb') as mHandle:
        max20ShortProfsLastYr = pickle.load(mHandle)

    with open('profs4All.pickle', 'rb') as rHandle:
        profs4All = pickle.load(rHandle)

#    print "symList out"
#    print symList
    dowFile = open("/home/wailoktam/tradeData/HistoricalPrices.csv", "r")
    n225FileList = ["/home/wailoktam/tradeData/n225/n225-2000.csv","/home/wailoktam/tradeData/n225/n225-2001.csv",\
                    "/home/wailoktam/tradeData/n225/n225-2002.csv", "/home/wailoktam/tradeData/n225/n225-2003.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2004.csv", "/home/wailoktam/tradeData/n225/n225-2005.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2006.csv","/home/wailoktam/tradeData/n225/n225-2007.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2008.csv", "/home/wailoktam/tradeData/n225/n225-2009.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2010.csv", "/home/wailoktam/tradeData/n225/n225-2011.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2012.csv","/home/wailoktam/tradeData/n225/n225-2013.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2014.csv","/home/wailoktam/tradeData/n225/n225-2015.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2016.csv"]
#    commoddict = make_commod_dict(shipfile,2)

    dowOpens, dowCloses,dowHighs, dowLows  = makeDowDicts(dowFile)
    n225Dict = n225DictFunct(n225FileList)

#    shipfile.close()
#    print n225Dict
#    print dowOpens
    dowFile.close()
#    intraday_dict_lists = intraday_dicts(all)
#    carry_dict_lists = carry_dicts(intraday_dict_lists[0],all)
#    formula_list = corr_cal([commoddict,dowdict],carry_dict_lists)
#    vhsd_list = vhsd_cal(all)

    profs4All, realProfs, maxDrawDn, pf =  yearLooper(startYear, profs4All, realProfs, max20ShortProfsLastYr, symList,dowOpens, dowCloses,dowHighs, dowLows,n225Dict, budget,bugSym)
    #            s_ri = s_ri - tesuuryou(daikin)
#            if s_ri-old_s_ri>0:
#                s_katsuhi=s_katsuhi+1
#            elif s_ri-old_s_ri<0:
#                s_makehi=s_makehi+1

    totalProfAll = sum(profs4All.values())
    totalProfSel = sum(realProfs.values())
#    print "s katsu/make/wake"
#    print s_katsu
#    print s_make
#    print s_wake
    print("All Symbols:",totalProfAll, allKatsu,allMake,((1.0*allKatsu)+(0.5*allWake))/(allKatsu+allMake+allWake),totalProfAll/(allKatsu+allWake+allMake), maxDrawDn)
    print("Selected Symbols:", totalProfSel, selKatsu, selMake, ((1.0 * selKatsu)+(0.5*selWake)) / (selKatsu + selMake+selWake),totalProfSel/ (selKatsu + selMake + selWake), maxDrawDn, pf)
    orderedAnnualPL = collections.OrderedDict(sorted(annualPL.items()))
    for k, v in orderedAnnualPL.iteritems(): print k, v


#no loss cut
#('Selected Symbols:', 43432500.0, 734, 459, 0.5968992248062015, 30607.82241014799)


# if rise > 1.005
#('All Symbols:', 87551100.0, 32716, 29604, 0.5229147031102733, 1289.3364161168709)
#('Selected Symbols:', 1086400.0, 331, 286, 0.532656023222061, 1576.7779390420899)





#   print("sell: day-based statistics",s_katsuhi,s_makehi,1.0*s_katsuhi/(s_katsuhi+s_makehi),s_ri/(s_katsuhi+s_makehi))
 #       print("buy: trade-based statistics",b_katsu,b_make,b_ri,1.0*b_katsu/(b_katsu+b_make+s_wake),b_ri/(b_katsu+b_make+b_wake))
 #       print("buy: day-based statistics",b_katsuhi,b_makehi,1.0*b_katsuhi/(b_katsuhi+b_makehi),b_ri*1.0/(b_katsuhi+b_makehi))
#    return(s_ri+b_ri)
    


#if __name__ == "__main__":
#    myproblem = Problem("opt_ship", 1, 0, mybounds, ship, myconstraint)
#    so = SimpleOpt(myproblem, 100000, 1, 5, 0, 0)
#    ship(so.optsearch(),1)

#a = [2]
#print ship(a,1)
    plotPL(monthlyPL,"monthlyPL.pdf")
    plotPL(annualPL,"annualPL.pdf")









with open('monthlyPL.pickle', 'wb') as handle:
    pickle.dump(monthlyPL, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('annualPL.pickle', 'wb') as handle:
    pickle.dump(annualPL, handle, protocol=pickle.HIGHEST_PROTOCOL)

#max20ShortProfs1stYr = dict(sorted(profs.iteritems(), key=operator.itemgetter(1), reverse=True)[:20])

#with open('max10ShortProfs1Yr.pickle', 'wb') as handle:
#    pickle.dump(max10ShortProfs1stYr, handle, protocol=pickle.HIGHEST_PROTOCOL)

#with open('bugSym.pickle', 'wb') as handle:
#    pickle.dump(bugSym, handle, protocol=pickle.HIGHEST_PROTOCOL)

debugFile.close()