      #!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import codecs
import copy
import re
import os
import datetime
import time
import numpy
import scipy
import pickle
import operator
from datetime import timedelta, date, datetime
from decimal import Decimal
from scipy import stats, optimize
import dateutil.parser
import matplotlib
import collections
from statistics import mean, stdev, pstdev
matplotlib.use('Agg')
from matplotlib import pyplot, dates

import math
from math import sqrt

patcomma = re.compile(r",", re.DOTALL)
patcolon = re.compile(r":", re.DOTALL)
patdot = re.compile(r"\.", re.DOTALL)
patNonIsoDateN225 = re.compile(r"(\d\d\d\d)(\d\d)(\d\d)", re.DOTALL)
patNonIsoDateDow = re.compile(r"(\d\d)\/(\d\d)\/(\d\d)", re.DOTALL)
patLast2Digit = re.compile(r"\d\d(\d\d)", re.DOTALL)

with open('bugSym.pickle', mode='rb') as handle:
    bugSym = pickle.load(handle)

ccounter = 0

debugFile = open("debug", "w")

delta = timedelta(days=1)


def z(x, meanX, sdX):
    return (x-meanX)/sdX

def linest(pairs):
    x_seq = tuple(p[0] for p in pairs)
    y_seq = tuple(p[1] for p in pairs)
    meanX, sdX = mean(x_seq), stdev(x_seq)
    meanY, sdY = mean(y_seq), stdev(y_seq)
    z_x = (z(x, meanX, sdX) for x in x_seq)
    z_y = (z(y, meanY, sdY) for y in y_seq)
    r_xy = sum( zx*zy for zx, zy in zip(z_x, z_y)) /len(pairs)
    beta = r_xy * sdY/sdX
    alpha = meanY- beta*meanX
    return r_xy, alpha, beta

def polyfit(x, y, degree):
#    results = {}

    coeffs = numpy.polyfit(x, y, degree)
     # Polynomial Coefficients
#    results['polynomial'] = coeffs.tolist()
    # r-squared
    p = numpy.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = numpy.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = numpy.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = numpy.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    return ssreg / sstot


def push(someList, item):
    someList = numpy.roll(someList, 1)
#    print ("someList")
#    print (someList)
#    print ("someList0")
#   print (someList[0])
#    print ("item")
#    print item
    someList[0] = item
    return someList


def makeSymList(symFile):
    latestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2017/y171027.txt", "rb")
    earliestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2000/y000104.txt", "rb")
    symList = []
    latestList = []
    earliestList = []
    symLines = symFile.readlines()

    latestLines = latestPriceFile.readlines()
    earliestLines = earliestPriceFile.readlines()
    for l in latestLines:
        sym = l.split("\t")[0].strip()
        latestList.append(sym)
    # print latestList
    for e in earliestLines:
        sym = e.split("\t")[0].strip()
        #        print "sym in earliest"
        #        print sym
        earliestList.append(sym)
    # print earliestList
    for s in symLines:
        sym = s.strip()
        symList.append(sym)
    return (set(latestList) & set(earliestList))


def makeLotSizeDict(symList):
    lotsizefile = open("/home/wailoktam/tradeData/stock.csv", "r")
    lotsizedict = {}
    lotsizelines = lotsizefile.readlines()
    for lotsizeline in lotsizelines:
        lotsizes = patcomma.split(lotsizeline)
        sym = lotsizes[0].strip()
        if sym in symList:
            lotsizedict[sym] = lotsizes[6].strip()
    lotsizefile.close()
    return (lotsizedict)


def tick_cal(price):
    if price < 2000:
        tick = 1
    elif price >= 2000 and price < 3000:
        tick = 5
    elif price >= 3000 and price < 30000:
        tick = 10
    elif price >= 30000 and price < 50000:
        tick = 50
    elif price >= 50000 and price < 100000:
        tick = 100
    elif price >= 100000 and price < 1000000:
        tick = 1000
    elif price >= 1000000 and price < 20000000:
        tick = 10000
    elif price >= 20000000 and price < 30000000:
        tick = 50000
    else:
        tick = 100000
    return (tick)


def date_generator(start_date, end_date, delta):
    d = start_date
    while d <= end_date:
        yield d
        d += delta


def neg_date_generator(start_date, end_date, delta):
    d = start_date
    while d >= end_date:
        yield d
        d -= delta


def convertDateFormat4N225(dateStr):
    matchDateStr = patNonIsoDateN225.search(dateStr)
    isoStr = matchDateStr.group(1) + "-" + matchDateStr.group(2) + "-" + matchDateStr.group(3)
    return (isoStr)


def convertDateFormat4Dow(dateStr):
    matchDateStr = patNonIsoDateDow.search(dateStr)
    isoStr = "20" + matchDateStr.group(3) + "-" + matchDateStr.group(1) + "-" + matchDateStr.group(2)
    return (isoStr)


def n225DictFunct(n225FileList):
    n225Dict = {}
    for fN in n225FileList:
        f = open(fN, "rb")
        n225Lines = f.readlines()
        for nL in n225Lines:
            n = patcomma.split(nL)
            dayStr = n[0].strip()
            n225Dict[convertDateFormat4N225(dayStr)] = n[4].strip()
    return (n225Dict)

def nxDictFunct(nxFile):
    nxDict = {}
    nxLines = nxFile.readlines()
    for nL in nxLines:
        n = patcomma.split(nL)
        dayStr = n[0].strip()
        nxDict[dayStr] = n[4].strip()
    return (nxDict)


def entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow, dowPrevPrevHike, beta):
    prevDowPlusOrMinusSpec = float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow)) / 2
    prevDowChange = (float(dowPrevHike) - float(dowPrevPrevHike)) / float(dowPrevPrevHike)
    #    prevDowChange = (float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow))/2)/((float(dowPrevHigh) + float(dowPrevLow))/2)
    #    debugFile.write("prevDowPlusOrMinus\n")
    #    debugFile.write(str(prevDowPlusOrMinusSpec)+"\n")
    #    debugFile.write("dowPrevPrevHike*1.02\n")
    ##    debugFile.write(str(float(dowPrevPrevHike)*1.002)+"\n")
    #    debugFile.write("dowPrevHike in entry_cal\n")
    #    debugFile.write(str(float(dowPrevHike))+"\n")
    if prevDowPlusOrMinusSpec > 0 and float(dowPrevHike) > (float(dowPrevPrevHike) * 1.005):
        entryPrice = float(prevHike) * (1 + prevDowChange*beta)
        if entryPrice < 2000: entryPrice = round(entryPrice)
        elif entryPrice >= 2000 and entryPrice < 3000:  entryPrice = round(entryPrice/5)*5
        elif entryPrice >= 3000 and entryPrice < 30000: entryPrice = round(entryPrice,-1)
        elif entryPrice >= 30000 and entryPrice < 50000: entryPrice = round(entryPrice/5,-1)*5
        elif entryPrice >= 50000 and entryPrice < 100000: entryPrice = round(entryPrice,-2)
        elif entryPrice >= 100000 and entryPrice < 1000000: entryPrice = round(entryPrice,-3)
        elif entryPrice >= 1000000 and entryPrice < 20000000: entryPrice = round(entryPrice,-4)
        elif entryPrice >= 20000000 and entryPrice < 30000000: entryPrice = round(entryPrice/5,-4)*5
        else: entryPrice = round(entryPrice,-5)
        entryPrice = entryPrice + tick_cal(entryPrice)
    else:
        entryPrice = 0
    return (entryPrice)


def exit_cal(entryPrice, beta):
    exitPrice = entryPrice * (1+(0.03*beta))
    if exitPrice < 2000:
        exitPrice = round(exitPrice)
    elif exitPrice >= 2000 and exitPrice < 3000:
        exitPrice = round(exitPrice / 5) * 5
    elif exitPrice >= 3000 and exitPrice < 30000:
        exitPrice = round(exitPrice, -1)
    elif exitPrice >= 30000 and exitPrice < 50000:
        exitPrice = round(exitPrice / 5, -1) * 5
    elif exitPrice >= 50000 and exitPrice < 100000:
        exitPrice = round(exitPrice, -2)
    elif exitPrice >= 100000 and exitPrice < 1000000:
        exitPrice = round(exitPrice, -3)
    elif exitPrice >= 1000000 and exitPrice < 20000000:
        exitPrice = round(exitPrice, -4)
    elif exitPrice >= 20000000 and exitPrice < 30000000:
        exitPrice = round(exitPrice / 5, -4) * 5
    else:
        exitPrice = round(exitPrice, -5)
    exitPrice = exitPrice + tick_cal(exitPrice)
#    exitPrice = 0
    return (exitPrice)


def sort(list, field):
    res = []  # always returns a list
    for x in list:
        i = 0
        for y in res:
            if x[field] <= y[field]: break  # list node goes here?
            i = i + 1
        res[i:i] = [x]  # insert in result slot
    return res



def makeSymPriceDict(symList, fullPath):
    symOpens = {}
    symCloses = {}
    symHighs = {}
    symLows = {}
    vol = {}
    file = open(fullPath, "rb")
    for l in file.readlines():
        if l.split("\t")[0] in symList:
            symOpens[l.split("\t")[0]] = float(l.split("\t")[2])
            symCloses[l.split("\t")[0]] = float(l.split("\t")[5])
            symHighs[l.split("\t")[0]] = float(l.split("\t")[3])
            symLows[l.split("\t")[0]] = float(l.split("\t")[4])
            try:
                vol[l.split("\t")[0]] = float(l.split("\t")[6])
            except ValueError:
                pass
    file.close()
    return (symOpens, symCloses, symHighs, symLows, vol)


def int_s_lc_mo(isoDate, symList, profs, betas, nxDict, prevIsoDate, prevPrevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget, bugSym, katsu, make, wake, betaOpt):
    daikin = 0
    mcounter = 0


    profOpt = 0


    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    fullPath = folderPath + "/" + filePath
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath

    prevPrevLast2Digit = patLast2Digit.search(prevPrevIsoDate.split("-")[0]).group(1)
    prevPrevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevPrevIsoDate.split("-")[0]
    prevPrevFilePath = "y" + prevPrevLast2Digit + prevPrevIsoDate.split("-")[1] + prevPrevIsoDate.split("-")[2] + ".txt"
    prevPrevFullPath = prevPrevFolderPath + "/" + prevPrevFilePath



    try:
        symOpens, symCloses, symHighs, _, vols = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        prevSymOpens, prevSymCloses, _, _, prevVols = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0

    try:
        _, prevPrevSymCloses, _, _, _ = makeSymPriceDict(symList, prevPrevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0


    entry = 0
    #    print "stockPriceFound"
    #    print stockPriceFileFound
    if stockPriceFileFound == 1:
        #       for s in ["4704"]:
#        print "symList"
#        print symList
        for s in symList:

            #            try:
            #            if s in max20ShortProfs1stYr:
            if s not in bugSym:

                prevNxHike = nxDict[prevIsoDate]
                nxHike = nxDict[isoDate]

                prevPrevHike = prevPrevSymCloses[s]



                prevHike = prevSymCloses[s]

                prevYori = prevSymOpens[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]
                vol = vols[s]
                lotSize = float(lotSizeDict[s])


                r2 = polyfit([i[0] for i in betas[s]], [i[1] for i in betas[s]], 1)

                if r2 > 0.5:
                    beta = linest(betas[s])[2]
                else:
                    beta = 1



#                gap = (prevHike -yori) * 1.0


#                if s in unitChange.keys():
#                    print "hit1"
#                    print unitChange[s][0]
#                    print dateutil.parser.parse(isoDate)
#                    if unitChange[s][0] > dateutil.parser.parse(isoDate):
#                        print"hit2"
#                        lotSize =  unitChange[s][1]


                prevVol = prevVols[s]
                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike, beta)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0 and not prevHike <= prevYori * 0.98:
#                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1

                elif high >= minEntryPrice and not minEntryPrice == 0 and not prevHike <= prevYori * 0.98:
#                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

                if entry == 1:

                    maxExitPrice = exit_cal(entryPrice, beta)

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not profs.get(s, 0) == 0:
                        if entryPrice * float(lotSize) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 0
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf = (entryPrice - tejimai) * lotSize
                                profs[s] = profs[s] + sameDayProf

                            else:
                                profOpt = 1
                                sameDayProf = (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = profs[s] + sameDayProf

                        else:
                            profOpt = 2
                            sameDayProf =  ((entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget))
                            profs[s] = profs[s] + sameDayProf
                    else:
                        if entryPrice * lotSize <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 3
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf =  (entryPrice - tejimai) * lotSize
                                profs[s] = sameDayProf
                            else:
                                profOpt = 4
                                sameDayProf =  (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = sameDayProf

                        else:
                            profOpt = 5
                            sameDayProf = (entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget)
                            profs[s] = sameDayProf

#                        lastclose = (string.atoi(prev[len(prev) - 1][4])) * 1000
#                        yori = (string.atoi(temp[0][1])) * 1000

#                        lc = round((yori * 1.03), -3)
#                        rcounter = 0
#                        while 1:







                    # daikin = daikin + entry
                    if entryPrice - tejimai >  0 and not (budget / hike * 10) > (vol * 1000):
                        katsu = katsu + 1
                    elif entryPrice - tejimai < 0 and not (budget / hike * 10) > (vol * 1000):
                        make = make + 1
                    else:
                        if not (budget / hike * 10) > (vol * 1000):
                            wake = wake + 1
#                    if dbgOpt == "debug":
#                        if sameDayProf > 100000 or sameDayProf < -100000:
                    debugFile.write(isoDate + "\n")
                    debugFile.write("symbol\n")
                    debugFile.write(str(s) + "\n")
                    debugFile.write("yori\n")
                    debugFile.write(str(yori) + "\n")
                    debugFile.write("hike\n")
                    debugFile.write(str(hike) + "\n")
                    debugFile.write("high\n")
                    debugFile.write(str(high) + "\n")
                    debugFile.write("prevHike\n")
                    debugFile.write(str(prevHike) + "\n")
                    debugFile.write("minEntry\n")
                    debugFile.write(str(minEntryPrice) + "\n")
                    debugFile.write("entry\n")
                    debugFile.write(str(entry) + "\n")
                    debugFile.write("lotsize\n")
                    debugFile.write(str(lotSize) + "\n")
                    debugFile.write("vol\n")
                    debugFile.write(str(vol*1000) + "\n")
                    debugFile.write("entryPice\n")
                    debugFile.write(str(entryPrice) + "\n")
                    debugFile.write("tejimai\n")
                    debugFile.write(str(tejimai) + "\n")
                    debugFile.write("sameDayProf\n")
                    debugFile.write(str(sameDayProf)+ "\n")
                    debugFile.write("profOpt\n")
                    debugFile.write(str(profOpt) + "\n")
                    debugFile.write("beta\n")
                    debugFile.write(str(beta) + "\n")

                if betaOpt == "betaCalc":
                    if s in betas:
                        if len(betas[s]) < 100:
                            betas[s] = numpy.append(betas[s],
                            numpy.array([((float(nxHike) / float(prevNxHike)) - 1,(hike / prevHike) - 1)]))

                        else:
                            betas[s] = push(betas[s], ((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1))
                    else:
                        betas[s] = numpy.array([((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1)],
                                                   dtype=object)



                            #            except KeyError:
                    #                bugSym[s] = ""
                    #                pass
    return (profs, katsu, make, wake, betas)



def int_s_lc(isoDate, symList, profs, prevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget, bugSym, katsu, make, wake, dbgOpt):
    daikin = 0
    mcounter = 0


    profOpt = 0


    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath
    fullPath = folderPath + "/" + filePath

    try:
        symOpens, symCloses, symHighs, _, vols = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        prevSymOpens, prevSymCloses, _, _, prevVols = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0


    entry = 0
    #    print "stockPriceFound"
    #    print stockPriceFileFound
    if stockPriceFileFound == 1:
        #       for s in ["4704"]:
#        print "symList"
#        print symList
        for s in symList:

            #            try:
            #            if s in max20ShortProfs1stYr:
            if s not in bugSym:
#                print "prevSymCloses"
#                print prevSymCloses
#                try:
#                    pass
#                except KeyError:
#                    print isoDate
#                    print symOpens
                prevHike = prevSymCloses[s]
                prevYori = prevSymOpens[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]
                vol = vols[s]
                lotSize = float(lotSizeDict[s])


#                gap = (prevHike -yori) * 1.0


#                if s in unitChange.keys():
#                    print "hit1"
#                    print unitChange[s][0]
#                    print dateutil.parser.parse(isoDate)
#                    if unitChange[s][0] > dateutil.parser.parse(isoDate):
#                        print"hit2"
#                        lotSize =  unitChange[s][1]


                prevVol = prevVols[s]
                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

#                print "symbol"
#                print s
#                print "yori"
#                print yori
#                print "hike"
#                print hike
#                print "high"
#                print high
#                print "prevHike"
#                print prevHike
#                print "minEntry"
#                print minEntryPrice
#                print "entry"
#                print entry


#                debugFile.write("dowPrevyori\n")
#                debugFile.write(str(dowPrevYori) + "\n")
#                debugFile.write("dowPrevhike\n")
#                debugFile.write(str(dowPrevHike) + "\n")
#                debugFile.write("dowPrevhigh\n")
#                debugFile.write(str(dowPrevHigh) + "\n")
#                debugFile.write("dowPrevLow\n")
#                debugFile.write(str(dowPrevLow) + "\n")
#                debugFile.write("dowPrevprevHike\n")
#                debugFile.write(str(dowPrevPrevHike) + "\n")

                if entry == 1:

                    maxExitPrice = exit_cal(entryPrice)

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not profs.get(s, 0) == 0:
                        if entryPrice * float(lotSize) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 0
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf = (entryPrice - tejimai) * lotSize
                                profs[s] = profs[s] + sameDayProf

                            else:
                                profOpt = 1
                                sameDayProf = (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = profs[s] + sameDayProf

                        else:
                            profOpt = 2
                            sameDayProf =  ((entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget))
                            profs[s] = profs[s] + sameDayProf
                    else:
                        if entryPrice * lotSize <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 3
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf =  (entryPrice - tejimai) * lotSize
                                profs[s] = sameDayProf
                            else:
                                profOpt = 4
                                sameDayProf =  (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = sameDayProf

                        else:
                            profOpt = 5
                            sameDayProf = (entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget)
                            profs[s] = sameDayProf

#                        lastclose = (string.atoi(prev[len(prev) - 1][4])) * 1000
#                        yori = (string.atoi(temp[0][1])) * 1000

#                        lc = round((yori * 1.03), -3)
#                        rcounter = 0
#                        while 1:







                    # daikin = daikin + entry
                    if entryPrice - tejimai >  0 and not (budget / hike * 10) > (vol * 1000):
                        katsu = katsu + 1
                    elif entryPrice - tejimai < 0 and not (budget / hike * 10) > (vol * 1000):
                        make = make + 1
                    else:
                        if not (budget / hike * 10) > (vol * 1000):
                            wake = wake + 1
                    if dbgOpt == "debug":
#                        if sameDayProf > 100000 or sameDayProf < -100000:
                            debugFile.write(isoDate + "\n")
                            debugFile.write("symbol\n")
                            debugFile.write(str(s) + "\n")
                            debugFile.write("yori\n")
                            debugFile.write(str(yori) + "\n")
                            debugFile.write("hike\n")
                            debugFile.write(str(hike) + "\n")
                            debugFile.write("high\n")
                            debugFile.write(str(high) + "\n")
                            debugFile.write("prevHike\n")
                            debugFile.write(str(prevHike) + "\n")
                            debugFile.write("minEntry\n")
                            debugFile.write(str(minEntryPrice) + "\n")
                            debugFile.write("entry\n")
                            debugFile.write(str(entry) + "\n")
                            debugFile.write("lotsize\n")
                            debugFile.write(str(lotSize) + "\n")
                            debugFile.write("vol\n")
                            debugFile.write(str(vol*1000) + "\n")
                            debugFile.write("entryPice\n")
                            debugFile.write(str(entryPrice) + "\n")
                            debugFile.write("tejimai\n")
                            debugFile.write(str(tejimai) + "\n")
                            debugFile.write("sameDayProf\n")
                            debugFile.write(str(sameDayProf)+ "\n")
                            debugFile.write("profOpt\n")
                            debugFile.write(str(profOpt) + "\n")


                    #            except KeyError:
                    #                bugSym[s] = ""
                    #                pass
    return (profs, katsu, make, wake)





def int_s_hike_b(isoDate, symList, profs, prevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget, bugSym, katsu, make, wake, dbgOpt):
    daikin = 0
    mcounter = 0


    profOpt = 0


    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath
    fullPath = folderPath + "/" + filePath

    try:
        symOpens, symCloses, symHighs, _, vols = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        _, prevSymCloses, _, _, prevVols = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0


    entry = 0
    #    print "stockPriceFound"
    #    print stockPriceFileFound
    if stockPriceFileFound == 1:
        #       for s in ["4704"]:
#        print "symList"
#        print symList
        for s in symList:

            #            try:
            #            if s in max20ShortProfs1stYr:
            if s not in bugSym:
#                print "prevSymCloses"
#                print prevSymCloses
#                try:
#                    pass
#                except KeyError:
#                    print isoDate
#                    print symOpens
                prevHike = prevSymCloses[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]
                vol = vols[s]
                lotSize = float(lotSizeDict[s])

#                if s in unitChange.keys():
#                    print "hit1"
#                    print unitChange[s][0]
#                    print dateutil.parser.parse(isoDate)
#                    if unitChange[s][0] > dateutil.parser.parse(isoDate):
#                        print"hit2"
#                        lotSize =  unitChange[s][1]


                prevVol = prevVols[s]
                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)

                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

                if entry == 1:
                    maxExitPrice = 0
#                    maxExitPrice = exit_cal(entryPrice)

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not profs.get(s, 0) == 0:
                        if entryPrice * float(lotSize) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 0
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf = (entryPrice - tejimai) * lotSize
                                profs[s] = profs[s] + sameDayProf

                            else:
                                profOpt = 1
                                sameDayProf = (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = profs[s] + sameDayProf

                        else:
                            profOpt = 2
                            sameDayProf =  ((entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget))
                            profs[s] = profs[s] + sameDayProf
                    else:
                        if entryPrice * lotSize <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 3
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf =  (entryPrice - tejimai) * lotSize
                                profs[s] = sameDayProf
                            else:
                                profOpt = 4
                                sameDayProf =  (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = sameDayProf

                        else:
                            profOpt = 5
                            sameDayProf = (entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget)
                            profs[s] = sameDayProf

                    # daikin = daikin + entry
                    if entryPrice - tejimai >  0 and not (budget / hike * 10) > (vol * 1000):
                        katsu = katsu + 1
                    elif entryPrice - tejimai < 0 and not (budget / hike * 10) > (vol * 1000):
                        make = make + 1
                    else:
                        if not (budget / hike * 10) > (vol * 1000):
                            wake = wake + 1
                    if dbgOpt == "debug":
#                        if sameDayProf > 100000 or sameDayProf < -100000:
                            debugFile.write(isoDate + "\n")
                            debugFile.write("symbol\n")
                            debugFile.write(str(s) + "\n")
                            debugFile.write("yori\n")
                            debugFile.write(str(yori) + "\n")
                            debugFile.write("hike\n")
                            debugFile.write(str(hike) + "\n")
                            debugFile.write("high\n")
                            debugFile.write(str(high) + "\n")
                            debugFile.write("prevHike\n")
                            debugFile.write(str(prevHike) + "\n")
                            debugFile.write("minEntry\n")
                            debugFile.write(str(minEntryPrice) + "\n")
                            debugFile.write("entry\n")
                            debugFile.write(str(entry) + "\n")
                            debugFile.write("lotsize\n")
                            debugFile.write(str(lotSize) + "\n")
                            debugFile.write("vol\n")
                            debugFile.write(str(vol*1000) + "\n")
                            debugFile.write("entryPice\n")
                            debugFile.write(str(entryPrice) + "\n")
                            debugFile.write("tejimai\n")
                            debugFile.write(str(tejimai) + "\n")
                            debugFile.write("sameDayProf\n")
                            debugFile.write(str(sameDayProf)+ "\n")
                            debugFile.write("profOpt\n")
                            debugFile.write(str(profOpt) + "\n")


                    #            except KeyError:
                    #                bugSym[s] = ""
                    #                pass
    return (profs, katsu, make, wake)




def tesuuryou(daikin):
    if daikin == 0:
        charge = 0
    elif daikin <= 500000:
        charge = 315
    elif daikin <= 1000000:
        charge = 840
    elif daikin <= 2000000:
        charge = 1680
    elif daikin <= 3000000:
        charge = 2520
    elif daikin <= 4000000:
        charge = 3360
    elif daikin <= 5000000:
        charge = 4200
    elif daikin <= 6000000:
        charge = 5040
    elif daikin <= 7000000:
        charge = 5880
    return (charge)


def makeDowDicts(file):
    dowOpens = {}
    dowCloses = {}
    dowHighs = {}
    dowLows = {}
    for dL in reversed(file.readlines()):
        dCells = patcomma.split(dL.strip())
        dowOpens[convertDateFormat4Dow(dCells[0])] = dCells[1]
        dowCloses[convertDateFormat4Dow(dCells[0])] = dCells[4]
        dowHighs[convertDateFormat4Dow(dCells[0])] = dCells[2]
        dowLows[convertDateFormat4Dow(dCells[0])] = dCells[3]
    # print dowDict
    return (dowOpens, dowCloses, dowHighs, dowLows)


def plotPL(plDict, figFileName):
    #    for d in plDict.keys():
    #        print "content of plDict"
    #        print d
    #        print "after apply strptime"
    #       e = (datetime.strptime(d, "%Y-%m-%d"))
    #        print e
    #        print "after apply date2num"
    #        print matplotlib.dates.date2num(e)

    x = matplotlib.dates.date2num([datetime.strptime(d, "%Y-%m-%d") for d in sorted(plDict)])
    y = [plDict[d]/(1000000*20) for d in sorted(plDict)]
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    ax.plot(x, y)
    years = matplotlib.dates.YearLocator()
    yearsFmt = matplotlib.dates.DateFormatter('%Y')
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(yearsFmt)
    fig.autofmt_xdate()
    pyplot.savefig(figFileName)

def difference_dict(Dict_A, Dict_B):
    output_dict = {}
    for key in Dict_A.keys():
        if key in Dict_B.keys():
            output_dict[key] = Dict_A[key] - Dict_B[key]
    return output_dict

#def debugYearLooper(year, realProfs, max20ShortProfsLastYr, symList, dowOpens, dowCloses, dowHighs, dowLows,

def last10YearProfWrapper(year, annualProfs):

    if year == 2001:
        outLast10 = annualProfs[2000]
    elif year == 2002:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] for k in annualProfs[2000]}
    elif year == 2003:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] for k in annualProfs[2000]}
    elif year == 2004:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k] for k in annualProfs[2000]}
    elif year == 2005:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k]  + annualProfs[2004][k]  for k
                    in annualProfs[2000]}
    elif year == 2006:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k]  + annualProfs[2004][k] + annualProfs[2005][k]  for k
                    in annualProfs[2000]}
    elif year == 2007:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k]  + annualProfs[2004][k] + annualProfs[2005][k]  + annualProfs[2006][k] for k
                    in annualProfs[2000]}
    elif year == 2008:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k]  + annualProfs[2004][k]  + annualProfs[2005][k]  + annualProfs[2006][k]  + annualProfs[2007][k] for k
                    in annualProfs[2000]}
    elif year == 2009:
        outLast10 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k] + annualProfs[2004][k]  + annualProfs[2005][k]  + annualProfs[2006][k]  + annualProfs[2007][k] +  + annualProfs[2008][k] for k
                    in annualProfs[2000]}
    else:
        outLast10 = {k: annualProfs[year-10][k] + annualProfs[year-9][k] + annualProfs[year-8][k] + annualProfs[year-7][k] + annualProfs[year-6][k] + annualProfs[year-5][k] + annualProfs[year-4][k] + annualProfs[year-3][k] + annualProfs[year-2][k] + annualProfs[year-1][k]for k in annualProfs[year-5]}

    return outLast10



def last5YearProfWrapper(year, annualProfs):

    if year == 2001:
        outLast5 = annualProfs[2000]
    elif year == 2002:
        outLast5 = {k: annualProfs[2000][k] + annualProfs[2001][k] for k in annualProfs[2000]}
    elif year == 2003:
        outLast5 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] for k in annualProfs[2000]}
    elif year == 2004:
        outLast5 = {k: annualProfs[2000][k] + annualProfs[2001][k] + annualProfs[2002][k] + annualProfs[2003][k] for k in annualProfs[2000]}
    else:
        outLast5 = {k: annualProfs[year-5][k] + annualProfs[year-4][k] + annualProfs[year-3][k] + annualProfs[year-2][k] + annualProfs[year-1][k] for k in annualProfs[year-5]}

    return outLast5



def last3YearProfWrapper(year, annualProfs):

    if year == 2001:
        outLast3 = annualProfs[2000]
    elif year == 2002:
        outLast3 = {k: annualProfs[2000][k] + annualProfs[2001][k] for k in annualProfs[2000]}
    else:
        outLast3 = {k: annualProfs[year-3][k] + annualProfs[year-2][k] + annualProfs[year-1][k] for k in annualProfs[year-3]}

    return outLast3


#def yearLooper(year, profs4All, realProfs, max20ShortProfsLastYr, symList, dowOpens, dowCloses, dowHighs, dowLows,

def yearLooper(year, profs4All, realProfs, max10ShortProfsLastYr, betaDict, nxDict, symList, dowOpens, dowCloses, dowHighs, dowLows,
               n225Dict, budget, bugSym):
    global allKatsu, allMake, allWake, selKatsu, selMake, selWake
    global monthlyPL
    global annualPL
    monthlyPL = {}
    annualPL = {}
 #   lossFlag = 0
    conscLoss = 0
    maxDrawDn = 0
    accuLoss = 0
    accuGain = 0

    annualProfs = {}

    #    year = 2001
    #    if year == 2004:
    while year <= 2018:
#    while year <= 2016:
        #        if year == 2000:
        #            firstTradeDay = 5
        #        else:
        firstTradeDay = 1
        firstTrade = 1
        for currentday in date_generator(date(year, 1, firstTradeDay), date(year, 12, 31), timedelta(days=1)):
            if firstTrade == 1:

                firstTrade = 0
            #        print currentday.isoformat()
            if n225Dict.has_key(currentday.isoformat()):
                yesterday225 = currentday - delta
                yesterday = currentday - delta

#                print yesterday
#                print yesterday225
                while 1:
                    if dowOpens.has_key(yesterday.isoformat()) and dowOpens.has_key(yesterday.isoformat()):
                        break
                    else:
                        #                   print yesterday
                        yesterday = yesterday - delta
                while 1:
                    if n225Dict.has_key(yesterday225.isoformat()):
                        break
                    else:
                        #                    print yesterday225
                        yesterday225 = yesterday225 - delta

                dayB4Yest = yesterday - delta
                dayB4Yest225 = yesterday225 - delta

                while 1:
                    if dowOpens.has_key(dayB4Yest.isoformat()):
                        break
                    else:
                        #                    print yesterday225
                        dayB4Yest = dayB4Yest - delta

                while 1:
                    if n225Dict.has_key(dayB4Yest225.isoformat()):
                        break
                    else:
                        dayB4Yest225 = dayB4Yest225 - delta

                        #            old_b_ri = b_ri
                        #            if string.atof(patcomma.split(filterdict[yesterday.isoformat()])[1]) < -0.002:
                        #                (b_ri,daikin)=int_b_hike_s(currentday.isoformat(),all,s_ri,yesterday225.isoformat(),commoddict[yesterday.isoformat()],dowdict[yesterday.isoformat()],formula_list,vhsd_list,x[0])
                        #                b_ri = b_ri - tesuuryou(daikin)
                        #                if b_ri-old_b_ri>0:
                        #                    b_katsuhi=b_katsuhi+1
                        #                elif b_ri-old_b_ri<0:
                        #                    b_makehi=b_makehi+1

                        #           old_s_ri = s_ri
                        #           print old_s_ri
                        #           if string.atof(patcomma.split(filterdict[yesterday.isoformat()])[1]) > 0.002:

                if not currentday.month == yesterday225.month:
                    monthlyPL[yesterday225.isoformat()] = sum(realProfs.values())
                # monthlyPL[yesterday225.isoformat()] = sum(profs4All.values())

                if not currentday.year == yesterday225.year:
                    annualPL[yesterday225.isoformat()] = sum(realProfs.values())
                # annualPL[yesterday225.isoformat()] = sum(profs4All.values())

                dPrevHike = dowCloses[yesterday.isoformat()]
                dPrevYori = dowOpens[yesterday.isoformat()]
                dPrevHigh = dowHighs[yesterday.isoformat()]

                dPrevLow = dowLows[yesterday.isoformat()]
                dPrevPrevHike = dowCloses[dayB4Yest.isoformat()]

                oldProfs4All = copy.deepcopy(profs4All)
                if year == 2001: annualProfs[2000] =oldProfs4All
                oldRealProfs = copy.deepcopy(realProfs)
                profs4All, allKatsu, allMake, allWake, betaDict = int_s_lc_mo(currentday.isoformat(), symList, profs4All, betaDict, nxDict,
                                                                     yesterday225.isoformat(), dayB4Yest225.isoformat(), dPrevYori,
                                                                     dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike,
                                                                     budget, bugSym, allKatsu, allMake, allWake, "BetaCalc")
#                realProfs, selKatsu, selMake, selWake = int_s_hike_b(currentday.isoformat(),
#                                                                     list(max20ShortProfsLastYr.keys()), realProfs,
#                                                                     yesterday225.isoformat(), dPrevYori,
#                                                                     dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike,
#                                                                     budget, bugSym, selKatsu, selMake, selWake, "debug")
                realProfs, selKatsu, selMake, selWake, betaDict = int_s_lc_mo(currentday.isoformat(),
                                                                     list(max10ShortProfsLastYr.keys()), realProfs, betaDict, nxDict,
                                                                     yesterday225.isoformat(), dayB4Yest225.isoformat(), dPrevYori,
                                                                     dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike,
                                                                     budget, bugSym, selKatsu, selMake, selWake, "noBetaCalc")



                debugFile.write("realProfs" + str(currentday.isoformat()) + "\n")
                debugFile.write(str(realProfs) + "\n")
                debugFile.write(str(sum(realProfs.values()))+ "\n")
                debugFile.write(str(selKatsu)+"/"+str(selMake)+"/"+str(selWake)+"\n")
                debugFile.write(str(sum(realProfs.values())/(selKatsu+selMake+selWake))+"\n")
                if sum(realProfs.values()) - sum(oldRealProfs.values())  > 0:
                    accuGain += (sum(realProfs.values()) - sum(oldRealProfs.values()))
                    conscLoss = 0
                    debugFile.write("accuGain"+str(currentday.isoformat())+"\n")
                    debugFile.write(str(accuGain)+"\n")
                    debugFile.write(str(sum(oldRealProfs.values()))+"\n")
                    debugFile.write(str(sum(realProfs.values()))+"\n")
                elif sum(realProfs.values()) - sum(oldRealProfs.values())  < 0:
                    accuLoss += (sum(oldRealProfs.values()) - sum(realProfs.values()))
                    conscLoss += (sum(oldRealProfs.values()) - sum(realProfs.values()))
                    debugFile.write("accuLoss"+str(currentday.isoformat())+"\n")
                    debugFile.write(str(accuLoss)+"\n")
                    debugFile.write(str(sum(oldRealProfs.values()))+"\n")
                    debugFile.write(str(sum(realProfs.values()))+"\n")




                if conscLoss > maxDrawDn:
                    maxDrawDn = conscLoss
                    debugFile.write("newDrawDown"+str(currentday.isoformat())+"\n")
#                    print (list(max20ShortProfsLastYr.keys()))
                    debugFile.write(str(list(max10ShortProfsLastYr.keys()))+"\n")
                    debugFile.write("conscloss" + str(conscLoss) + "\n")


        annualProfs[year] = {k: profs4All[k] - oldProfs4All[k] for k in oldProfs4All}


        max10ShortProfsLastYr = dict(
                    sorted(profs4All.iteritems(), key=operator.itemgetter(1), reverse=True)[:10])


        with open("max10ShortProfs"+str(year)+".pickle", 'wb') as mHandle:
            pickle.dump(max10ShortProfsLastYr, mHandle, protocol=pickle.HIGHEST_PROTOCOL)

        year += 1




        print "maxDrawDn: "+str(maxDrawDn)
        print "profFact: "+str(float(accuGain)/float(accuLoss))

    annualPL[yesterday225.isoformat()] = sum(realProfs.values())
    orderedAnnualPL = collections.OrderedDict(sorted(annualPL.items()))
    lastPL = 0
    for k, v in orderedAnnualPL.iteritems():
        diffPL = v - lastPL
        print "annualPL: " + k +"/" + str(diffPL)
        lastPL = v


    return profs4All, realProfs

def main():
    budget = 1000000
    startYear = 2001
    realProfs = {}

    global allKatsu, allMake, allWake, selKatsu, selMake, selWake, monthlyPL, annualPL
    allKatsu = 0
    allMake = 0
    allWake = 0
    selKatsu = 0
    selMake = 0
    selWake = 0

    board1SymListFile = open("/home/wailoktam/tradeData/tosho1.csv", "rb")
    symList = makeSymList(board1SymListFile)

    with open('betaDict.pickle', 'rb') as mHandle:
        betaDict = pickle.load(mHandle)

    with open('max10ShortProfs1Yr.pickle', 'rb') as mHandle:
        max10ShortProfsLastYr = pickle.load(mHandle)

    with open('profs4All.pickle', 'rb') as rHandle:

        profs4All = pickle.load(rHandle)


    dowFile = open("/home/wailoktam/tradeData/HistoricalPrices.csv", "r")
    nxFile = open("/home/wailoktam/tradeData/nx/nx.csv", "r")
    n225FileList = ["/home/wailoktam/tradeData/n225/n225-2000.csv", "/home/wailoktam/tradeData/n225/n225-2001.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2002.csv", "/home/wailoktam/tradeData/n225/n225-2003.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2004.csv", "/home/wailoktam/tradeData/n225/n225-2005.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2006.csv", "/home/wailoktam/tradeData/n225/n225-2007.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2008.csv", "/home/wailoktam/tradeData/n225/n225-2009.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2010.csv", "/home/wailoktam/tradeData/n225/n225-2011.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2012.csv", "/home/wailoktam/tradeData/n225/n225-2013.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2014.csv", "/home/wailoktam/tradeData/n225/n225-2015.csv", \
                        "/home/wailoktam/tradeData/n225/n225-2016.csv", "/home/wailoktam/tradeData/n225/n225-2017.csv"]
        #    commoddict = make_commod_dict(shipfile,2)

    dowOpens, dowCloses, dowHighs, dowLows = makeDowDicts(dowFile)
    n225Dict = n225DictFunct(n225FileList)
    nxDict = nxDictFunct(nxFile)


    dowFile.close()




    profs4All, realProfs = yearLooper(startYear, profs4All, realProfs, max10ShortProfsLastYr, betaDict, nxDict, symList, dowOpens,
                                          dowCloses, dowHighs, dowLows, n225Dict, budget, bugSym)



    totalProfAll = sum(profs4All.values())
    totalProfSel = sum(realProfs.values())

    print("All Symbols:", totalProfAll, allKatsu, allMake,
              ((1.0 * allKatsu) + (0.5 * allWake)) / (allKatsu + allMake + allWake),
              totalProfAll / (allKatsu + allWake + allMake))
    print("Selected Symbols:", totalProfSel, selKatsu, selMake,
              ((1.0 * selKatsu) + (0.5 * selWake)) / (selKatsu + selMake + selWake),
              totalProfSel / (selKatsu + selMake + selWake))

    plotPL(monthlyPL, "monthlyPL.pdf")
    plotPL(annualPL, "annualPL.pdf")

    with open('betaDict.pickle', 'wb') as mHandle:
        pickle.dump(betaDict, mHandle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('monthlyPL.pickle', 'wb') as handle:
        pickle.dump(monthlyPL, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('annualPL.pickle', 'wb') as handle:
        pickle.dump(annualPL, handle, protocol=pickle.HIGHEST_PROTOCOL)



    debugFile.close()




if __name__ == "__main__":
    main()

