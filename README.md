# README #


This is a rule-based trading system.  

## Logic


If Dow yesterday > Dow the day b4 yesterday * (1+Trigger):    
	Loop Until End of Day:    
    	If Sym Price > Sym Price * Dow yesterday/Dow the day b4 yesterday:     
			Sell    
			Position = Open    
		If Position == Open:    
		If Sym Price > Stop Loss:    
	    	Buyback    
	Buyback    

Trade highest ranking sym in back test    
 

### Setup 1

* Stake: 5000000 yen per symbol
* Symbol: 20 TSE1 Stocks
* Back test: 2000 - 2015 
* Forward test: 2001 - 2016
* Long/Short: Short only
* Trigger: 0.5% 
* Ranking: latest only

### Result for Setup 1

* Profit: 65722521
* Win: 6434
* Loss: 5430
* Win/Loss Ratio: 0.54
* Profit per Trade: 5362
* Annual Return: 4%

### Setup 2

* Stake: 5000000 yen per symbol
* Symbol: 20 TSE1 Stocks
* Back test: 2000 - 2015 
* Forward test: 2001 - 2016
* Long/Short: Short only
* Trigger: 0.5% 
* Ranking: acccmulate

### Result for Setup 2

* Profit: 218571085

* Win: 7799
* Loss: 5484
* Win/Loss Ratio: 0.58
* Profit per Trade: 15336
* Annual Return: 14%



