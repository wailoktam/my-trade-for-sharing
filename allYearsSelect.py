#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import codecs
import operator
import re
import os
import datetime
import time
import numpy
import scipy
import pickle
from datetime import timedelta, date
from decimal import Decimal
from scipy import stats, optimize

import math
from math import sqrt

# symList = ["9104"]
# all = ["9101","9104","9107","9110","9113","9115","9119","9123","9127","9130","9132","9152","9179"]
# slice_obj = numpy.s_[0:3:0.5]

patcomma = re.compile(r",", re.DOTALL)
patcolon = re.compile(r":", re.DOTALL)
patdot = re.compile(r"\.", re.DOTALL)
patNonIsoDateN225 = re.compile(r"(\d\d\d\d)(\d\d)(\d\d)", re.DOTALL)
patNonIsoDateDow = re.compile(r"(\d\d)\/(\d\d)\/(\d\d)", re.DOTALL)
patLast2Digit = re.compile(r"\d\d(\d\d)", re.DOTALL)

with open('max20ShortProfs1Yr.pickle', 'rb') as handle:
    max20ShortProfsLastYr = pickle.load(handle)
with open('bugSym.pickle', mode='rb') as handle:
    bugSym = pickle.load(handle)




#with open('bugSym.pickle', mode='rb') as handle:
#    bugSym = pickle.load(handle)

ccounter = 0

# mybounds = [ (0.001, 0.1) ]




delta = timedelta(days=1)


# def myconstraint(i, x):return(-1)

# def filterbydowdict():
#    dowfile = open("../dow/table.csv", "r")
#    dow1dict = {}
#    dowlines = dowfile.readlines()
#    for dowline in dowlines:
#        dows = patcomma.split(dowline)
#        tradeday = dows[0].strip()
#        dow1dict[tradeday] = dows[6].strip()+","+dows[7].strip()
#    dowfile.close()
#    return(dow1dict)



def makeSymList(symFile):
    latestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2016/y161230.txt", "rb")
    earliestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2000/y000104.txt", "rb")
    symList = []
    latestList = []
    earliestList = []
    symLines = symFile.readlines()
    #    print symLines
    latestLines = latestPriceFile.readlines()
    earliestLines = earliestPriceFile.readlines()
    for l in latestLines:
        sym = l.split("\t")[0].strip()
        latestList.append(sym)
    # print latestList
    for e in earliestLines:
        sym = e.split("\t")[0].strip()
        #        print "sym in earliest"
        #        print sym
        earliestList.append(sym)
    # print earliestList
    for s in symLines:
        sym = s.strip()
        symList.append(sym)
    return (set(symList) & set(latestList) & set(earliestList))


def makeLotSizeDict(symList):
    lotsizefile = open("/home/wailoktam/tradeData/stock.csv", "r")
    lotsizedict = {}
    lotsizelines = lotsizefile.readlines()
    for lotsizeline in lotsizelines:
        lotsizes = patcomma.split(lotsizeline)
        sym = lotsizes[0].strip()
        if sym in symList:
            lotsizedict[sym] = lotsizes[6].strip()
    lotsizefile.close()
    return (lotsizedict)


def tick_cal(price):
    if price < 2000:
        tick = 1
    elif price >= 2000 and price < 3000:
        tick = 5
    elif price >= 3000 and price < 30000:
        tick = 10
    elif price >= 30000 and price < 50000:
        tick = 50
    elif price >= 50000 and price < 100000:
        tick = 100
    elif price >= 100000 and price < 1000000:
        tick = 1000
    elif price >= 1000000 and price < 20000000:
        tick = 10000
    elif price >= 20000000 and price < 30000000:
        tick = 50000
    else:
        tick = 100000
    return (tick)


def date_generator(start_date, end_date, delta):
    d = start_date
    while d <= end_date:
        yield d
        d += delta


def neg_date_generator(start_date, end_date, delta):
    d = start_date
    while d >= end_date:
        yield d
        d -= delta


def convertDateFormat4N225(dateStr):
    matchDateStr = patNonIsoDateN225.search(dateStr)
    isoStr = matchDateStr.group(1) + "-" + matchDateStr.group(2) + "-" + matchDateStr.group(3)
    return (isoStr)


def convertDateFormat4Dow(dateStr):
    matchDateStr = patNonIsoDateDow.search(dateStr)
    isoStr = "20" + matchDateStr.group(3) + "-" + matchDateStr.group(1) + "-" + matchDateStr.group(2)
    return (isoStr)


def n225DictFunct(n225FileList):
    n225Dict = {}
    for fN in n225FileList:
        f = open(fN, "rb")
        n225Lines = f.readlines()
        for nL in n225Lines:
            n = patcomma.split(nL)
            dayStr = n[0].strip()
            n225Dict[convertDateFormat4N225(dayStr)] = n[4].strip()
    return (n225Dict)


def entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow, dowPrevPrevHike):
    prevDowPlusOrMinusSpec = float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow)) / 2
#    prevDowChange = (float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow)) / 2) / (
#    (float(dowPrevHigh) + float(dowPrevLow)) / 2)
    prevDowChange = (float(dowPrevHike) - float(dowPrevPrevHike))/float(dowPrevPrevHike)
    if prevDowPlusOrMinusSpec > 0 and float(dowPrevHike) > (float(dowPrevPrevHike) * 1.005):
        entryPrice = float(prevHike) * (1 + prevDowChange)
    else:
        entryPrice = 0
    return (entryPrice)


# def entry_cal(price,commod,dow,formula,vhsd,y,position):
#    if price < 2000: new_price = round(price)
#    elif price >= 2000 and price < 3000:  new_price = round(price/5)*5
#    elif price >= 3000 and price < 30000: new_price = round(price,-1)
#    elif price >= 30000 and price < 50000: new_price = round(price/5,-1)*5
#    elif price >= 50000 and price < 100000: new_price = round(price,-2)
#    elif price >= 100000 and price < 1000000: new_price = round(price,-3)
#    elif price >= 1000000 and price < 20000000: new_price = round(price,-4)
#    elif price >= 20000000 and price < 30000000: new_price = round(price/5,-4)*5
#    else: new_price = round(price,-5)
#    if position == "l":
#        while 1:
#            new_price = new_price - tick_cal(new_price)
#            if new_price <= price + formula[0][2]+string.atof(commod)*formula[0][0]+string.atof(dow)*formula[0][1]- y * vhsd:
#                break
#    elif position == "s":
#        while 1:
##            new_price = new_price + tick_cal(new_price)
#            if new_price >= price + formula[0][2]+string.atof(commod)*formula[0][0]+string.atof(dow)*formula[0][1]+ y * vhsd:
#                break
#    return(new_price)

def exit_cal(price, commod, dow, formula, vhsd, y, position):
    if price < 2000:
        new_price = round(price)
    elif price >= 2000 and price < 3000:
        new_price = round(price / 5) * 5
    elif price >= 3000 and price < 30000:
        new_price = round(price, -1)
    elif price >= 30000 and price < 50000:
        new_price = round(price / 5, -1) * 5
    elif price >= 50000 and price < 100000:
        new_price = round(price, -2)
    elif price >= 100000 and price < 1000000:
        new_price = round(price, -3)
    elif price >= 1000000 and price < 20000000:
        new_price = round(price, -4)
    elif price >= 20000000 and price < 30000000:
        new_price = round(price / 5, -4) * 5
    else:
        new_price = round(price, -5)
    if position == "l":
        while 1:
            new_price = new_price + tick_cal(new_price)
            if new_price >= price + formula[0][2] + string.atof(commod) * formula[0][0] + string.atof(dow) * formula[0][
                1] + y * vhsd:
                break
    elif position == "s":
        while 1:
            new_price = new_price - tick_cal(new_price)
            if new_price <= price + formula[0][2] + string.atof(commod) * formula[0][0] + string.atof(dow) * formula[0][
                1] - y * vhsd:
                break
    return (new_price)


def corr_cal(commoddict_list, carry_dict_list):
    stocklist = []
    pstocklist = []
    daylist = []
    resultlist = []
    storelist = []
    commodlistoflist = []
    stop = 0
    dcounter = 0
    checkfile = open("../Python/ship_check.csv", "w")
    for meigara in carry_dict_list:
        stocklist.append([])
        pstocklist.append([])
        daylist.append([])
    ccounter = 0
    for commoddict in commoddict_list:
        commodlistoflist.append([])
        storelist.append(0)
    for currentday in neg_date_generator(date(2008, 12, 25), date(2008, 1, 1), timedelta(days=1)):
        mcounter = 0
        if carry_dict_list[mcounter].has_key(currentday.isoformat()):
            while 1:
                #                print mcounter
                stocklist[mcounter].append(carry_dict_list[mcounter][currentday.isoformat()])
                daylist[mcounter].append(currentday.isoformat())
                mcounter = mcounter + 1
                if mcounter > (len(carry_dict_list) - 1): break
            yesterday = currentday - delta
            while 1:
                flag = 1

                for commoddict in commoddict_list:
                    if not commoddict.has_key(yesterday.isoformat()):
                        flag = 0
                if flag == 1:
                    ccounter = 0
                    for commoddict in commoddict_list:
                        storelist[ccounter] = storelist[ccounter] + string.atof(commoddict[yesterday.isoformat()])
                        ccounter = ccounter + 1
                    if carry_dict_list[0].has_key(yesterday.isoformat()):
                        ccounter = 0
                        for commoddict in commoddict_list:
                            commodlistoflist[ccounter].append(storelist[ccounter])
                            storelist[ccounter] = 0
                            ccounter = ccounter + 1
                        break
                else:
                    mcounter = 0
                    if carry_dict_list[mcounter].has_key(yesterday.isoformat()):
                        while 1:
                            stocklist[mcounter].pop()
                            daylist[mcounter].pop()
                            mcounter = mcounter + 1
                            if mcounter > (len(carry_dict_list) - 1): break
                        break
                if yesterday >= datetime.datetime.fromtimestamp(
                        time.mktime(time.strptime("2008-01-1", "%Y-%m-%d"))).date():
                    yesterday = yesterday - delta
                else:
                    ccounter = 0
                    for commoddict in commoddict_list:
                        commodlistoflist[ccounter].append(storelist[ccounter])
                        storelist[ccounter] = 0
                        ccounter = ccounter + 1
                    break

                    #    print [len(daylist[0]),len(stocklist[0]),len(commodlistoflist[0]),len(commodlistoflist[1])]
    for day in daylist[0]:
        #        print [stocklist[0][dcounter],commodlistoflist[0][dcounter],commodlistoflist[1][dcounter]]
        checkfile.write(
            day + "," + str((stocklist[0])[dcounter]) + "," + str((commodlistoflist[0])[dcounter]) + "," + str(
                (commodlistoflist[1])[dcounter]) + "\n")
        #        checkfile.write(day+","+((stocklist[0])[dcounter])+","+((commodlistoflist[0])[dcounter])+","+((commodlistoflist[1])[dcounter])+"\n")
        dcounter = dcounter + 1

    commod1array = scipy.array(commodlistoflist[0])
    commod2array = scipy.array(commodlistoflist[1])
    formula_list = []
    for i in range(len(carry_dict_list)):
        formula_list.append(
            numpy.linalg.lstsq(numpy.column_stack([commod1array, commod2array, numpy.ones(len(commod1array), float)]),
                               scipy.array(stocklist[i])))
        for j in range(len(commod1array)):
            pstocklist[i].append(
                formula_list[i][0][2] + commod1array[j] * formula_list[i][0][0] + commod2array[j] * formula_list[i][0][
                    1])
        print scipy.stats.pearsonr(scipy.array(stocklist[i]), scipy.array(pstocklist[i]))

    checkfile.close()
    return (formula_list)


def vhsd_cal(meigaralist):
    # standard deviation
    vhsd_list = []
    for meigara in meigaralist:
        vhsd_sum = 0
        vcounter = 0
        for currentday in date_generator(date(2008, 1, 1), date(2008, 12, 23), timedelta(days=1)):
            temp = []
            pricelist = []
            sumdiff = 0
            oldhour = "08"
            for shou in range(1, 5):
                if shou <= 3:
                    shou_cp = shou
                else:
                    shou_cp = 9
                for bu in range(1, 5):
                    shoubu = str(shou_cp) + str(bu)
                    day2 = "".join(currentday.isoformat().split("-"))
                    candlefilename = "../candles/" + day2 + "/" + meigara + "." + shoubu + ".csv"
                    try:
                        candlefile = open(candlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(candlefilename) > 0:
                            rows = candlefile.readlines()
                            for row in rows:
                                cells = patcomma.split(row)
                                temp.append(tuple(cells))
                                if patcolon.split(cells[0])[0] != oldhour: pricelist.append(string.atoi(cells[4]))
                                oldhour = patcolon.split(cells[0])[0]
                            candlefile.close()
            temp = sort(temp, 0)
            try:
                hike = string.atoi(temp[len(temp) - 1][4])
                for price in pricelist:
                    sumdiff = sumdiff + (price - hike) ** 2
                # print currentday,(price - hike)**2
                vhsd_sum = vhsd_sum + sqrt(sumdiff) / len(pricelist)
                #                print currentday,len(pricelist),sumdiff,sqrt(sumdiff)/len(pricelist)

                vcounter = vcounter + 1
            except IndexError:
                pass
        vhsd_list.append(vhsd_sum / vcounter)
    return (vhsd_list)


def carry_dicts(intraday_dict, meigaralist):
    carry_dict_list = []
    for meigara in meigaralist:
        unfinished_dict = {}
        for currentday in date_generator(date(2008, 1, 5), date(2008, 12, 23), timedelta(days=1)):
            yesterday = currentday - delta
            while 1:
                if intraday_dict.has_key(yesterday.isoformat()):
                    break
                else:
                    yesterday = yesterday - delta
            temp = []
            for shou in range(1, 5):
                if shou <= 3:
                    shou_cp = shou
                else:
                    shou_cp = 9
                for bu in range(1, 5):
                    shoubu = str(shou_cp) + str(bu)
                    day2 = "".join(currentday.isoformat().split("-"))
                    candlefilename = "../candles/" + day2 + "/" + meigara + "." + shoubu + ".csv"
                    try:
                        candlefile = open(candlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(candlefilename) > 0:
                            rows = candlefile.readlines()
                            for row in rows:
                                cells = patcomma.split(row)
                                temp.append(tuple(cells))
                        candlefile.close()
            temp = sort(temp, 0)
            try:
                prevtemp = []
                for shou in range(1, 5):
                    if shou <= 3:
                        shou_cp = shou
                    else:
                        shou_cp = 9
                    for bu in range(1, 5):
                        shoubu = str(shou_cp) + str(bu)
                        day3 = "".join(yesterday.isoformat().split("-"))
                        prevcandlefilename = "../candles/" + day3 + "/" + meigara + "." + shoubu + ".csv"
                        #                     print prevcandlefilename
                        try:
                            prevcandlefile = open(prevcandlefilename, "r")
                        except IOError:
                            pass
                        else:
                            if os.path.getsize(prevcandlefilename) > 0:
                                rows = prevcandlefile.readlines()
                                for row in rows:
                                    cells = patcomma.split(row)
                                    prevtemp.append(tuple(cells))
                            prevcandlefile.close()
                prevtemp = sort(prevtemp, 0)
                #            print prevtemp
                prevhike = (string.atoi(prevtemp[len(prevtemp) - 1][4]))
                #            yori = (string.atoi(temp[0][1]))*(string.atoi(subdict[meigara]))
                hike = (string.atoi(temp[len(temp) - 1][4]))
                #            min_entry = entry_cal(prevhike,dow_percent*Decimal(beta_dict[meigara]))
                carry = hike - prevhike
                unfinished_dict[currentday.isoformat()] = carry
            except IndexError:
                #            print meigara,prevcandlefilename
                pass
        carry_dict_list.append(unfinished_dict)
    return (carry_dict_list)


def intraday_dicts(meigaralist):
    intraday_dict_list = []
    for meigara in meigaralist:
        unfinished_dict = {}
        for currentday in date_generator(date(2008, 1, 1), date(2008, 12, 23), timedelta(days=1)):
            temp = []
            for shou in range(1, 5):
                if shou <= 3:
                    shou_cp = shou
                else:
                    shou_cp = 9
                for bu in range(1, 5):
                    shoubu = str(shou_cp) + str(bu)
                    day2 = "".join(currentday.isoformat().split("-"))
                    candlefilename = "../candles/" + day2 + "/" + meigara + "." + shoubu + ".csv"
                    try:
                        candlefile = open(candlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(candlefilename) > 0:
                            rows = candlefile.readlines()
                            for row in rows:
                                cells = patcomma.split(row)
                                temp.append(tuple(cells))
                        candlefile.close()
            temp = sort(temp, 0)
            try:
                yori = (string.atof(temp[0][1]))
                hike = (string.atof(temp[len(temp) - 1][4]))
                intraday = hike - yori
                unfinished_dict[currentday.isoformat()] = intraday
                candlefile.close()
            except IndexError:
                pass
        intraday_dict_list.append(unfinished_dict)
    return (intraday_dict_list)


def sort(list, field):
    res = []  # always returns a list
    for x in list:
        i = 0
        for y in res:
            if x[field] <= y[field]: break  # list node goes here?
            i = i + 1
        res[i:i] = [x]  # insert in result slot
    return res



def makeSymPriceDict(symList, fullPath):
    symOpens = {}
    symCloses = {}
    symHighs = {}
    symLows = {}
    file = open(fullPath, "rb")
    for l in file.readlines():
        if l.split("\t")[0] in symList:
            symOpens[l.split("\t")[0]] = float(l.split("\t")[2])
            symCloses[l.split("\t")[0]] = float(l.split("\t")[5])
            symHighs[l.split("\t")[0]] = float(l.split("\t")[3])
            symLows[l.split("\t")[0]] = float(l.split("\t")[4])
    file.close()
    return (symOpens, symCloses, symHighs, symLows)


def int_s_hike_b(isoDate, symList, shortProfs, prevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget):
    global allKatsu, allMake, allWake

    daikin = 0
    mcounter = 0

    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath
    fullPath = folderPath + "/" + filePath
    try:
        symOpens, symCloses, symHighs, _ = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        _, prevSymCloses, _, _ = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    entry = 0
    if stockPriceFileFound == 1:
        for s in symList:
            if s not in bugSym:
#            try:
                prevHike = prevSymCloses[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]

                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

                if entry == 1:

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not shortProfs.get(s, 0) == 0:
                        shortProfs[s] = shortProfs[s] + (entryPrice - tejimai) * float(lotSizeDict[s]) * (
                        round(budget / (entryPrice * float(lotSizeDict[s])) - 0.5))
                    else:
                        shortProfs[s] = (entryPrice - tejimai) * float(lotSizeDict[s]) * (
                        round(budget / (entryPrice * float(lotSizeDict[s])) - 0.5))
                    # daikin = daikin + entry
                    if entryPrice - tejimai > 0:
                        allKatsu = allKatsu + 1
                    elif entryPrice - tejimai < 0:
                        allMake = allMake + 1
                    else:
                        allWake = allWake + 1
#            except KeyError:
#                if not s in bugSym:
#                    bugSym[s] = ""

                    #      except IndexError:
                    #            pass
    return (shortProfs)


def int_b_hike_s(day1, meigaralist, b_ri, prev, commod, dow, formula_list, vhsd_list, y):
    global b_katsu, b_make, b_wake
    daikin = 0
    mcounter = 0
    #    subdict=lotsizedict()
    for meigara in meigaralist:
        temp = []
        for shou in range(1, 5):
            if shou <= 3:
                shou_cp = shou
            else:
                shou_cp = 9
            for bu in range(1, 5):
                shoubu = str(shou_cp) + str(bu)
                day2 = "".join(day1.split("-"))
                candlefilename = "../candles/" + day2 + "/" + meigara + "." + shoubu + ".csv"
                try:
                    candlefile = open(candlefilename, "r")
                except IOError:
                    pass
                else:
                    if os.path.getsize(candlefilename) > 0:
                        rows = candlefile.readlines()
                        for row in rows:
                            cells = patcomma.split(row)
                            temp.append(tuple(cells))
                    candlefile.close()
        temp = sort(temp, 0)
        try:
            prevtemp = []
            for shou in range(1, 5):
                if shou <= 3:
                    shou_cp = shou
                else:
                    shou_cp = 9
                for bu in range(1, 5):
                    shoubu = str(shou_cp) + str(bu)
                    day3 = "".join(prev.split("-"))
                    prevcandlefilename = "../candles/" + day3 + "/" + meigara + "." + shoubu + ".csv"
                    try:
                        prevcandlefile = open(prevcandlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(prevcandlefilename) > 0:
                            rows = prevcandlefile.readlines()
                            for row in rows:
                                cells = patcomma.split(row)
                                prevtemp.append(tuple(cells))
                        prevcandlefile.close()
            prevtemp = sort(prevtemp, 0)
            prevhike = (string.atoi(prevtemp[len(prevtemp) - 1][4]))
            yori = (string.atoi(temp[0][1])) * (string.atoi(subdict[meigara]))
            hike = (string.atoi(temp[len(temp) - 1][4])) * (string.atoi(subdict[meigara]))

            max_entry = entry_cal(prevhike, commod, dow, formula_list[mcounter], vhsd_list[mcounter], y, "l") * (
            string.atoi(subdict[meigara]))
            min_exit = exit_cal(prevhike, commod, dow, formula_list[mcounter], vhsd_list[mcounter], y, "l") * (
            string.atoi(subdict[meigara]))

            if yori <= max_entry:
                entry = yori
                action = "entry"
                rcounter = 0
                while 1:
                    if (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) <= min_exit) and (
                            string.atoi(temp[rcounter][2]) * (string.atoi(subdict[meigara])) > min_exit):
                        tejimai = min_exit
                        break
                    elif (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) > min_exit):
                        tejimai = string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara]))
                        break
                    else:
                        if rcounter == len(temp) - 1:
                            tejimai = string.atoi(temp[len(temp) - 1][4]) * (string.atoi(subdict[meigara]))
                            break
                    rcounter = rcounter + 1
            else:
                rcounter = 0
                action = "noentry"
                while 1:
                    if (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) < max_entry):
                        entry = string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara]))
                        action = "entry"
                        entry_pt = rcounter
                        break
                    elif (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) >= max_entry) and (
                            string.atoi(temp[rcounter][2]) * (string.atoi(subdict[meigara])) < max_entry):
                        entry = max_entry
                        action = "entry"
                        entry_pt = rcounter
                        break
                    if rcounter == len(temp) - 1:
                        break
                    rcounter = rcounter + 1
                rcounter = 0
                if action == "entry":
                    while 1:
                        if (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) <= min_exit) and (
                                string.atoi(temp[rcounter][2]) * (
                            string.atoi(subdict[meigara])) > min_exit) and rcounter > entry_pt:
                            tejimai = min_exit
                            break
                        elif (string.atoi(temp[rcounter][1]) * (
                        string.atoi(subdict[meigara])) > min_exit) and rcounter > entry_pt:
                            tejimai = string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara]))
                            break
                        else:
                            if rcounter == len(temp) - 1:
                                tejimai = string.atoi(temp[len(temp) - 1][4]) * (string.atoi(subdict[meigara]))
                                break
                        rcounter = rcounter + 1

            if action != "noentry":
                b_ri = b_ri + (tejimai - entry)
                daikin = daikin + entry
                if tejimai - entry > 0:
                    b_katsu = b_katsu + 1
                elif tejimai - entry < 0:
                    b_make = b_make + 1
                else:
                    b_wake = b_wake + 1
            # print(day1,meigara,entry,tejimai,entry-tejimai,min_entry)
            candlefile.close()
            prevcandlefile.close()
        except IndexError:
            pass
        mcounter = mcounter + 1
    return ((b_ri, daikin))


def tesuuryou(daikin):
    if daikin == 0:
        charge = 0
    elif daikin <= 500000:
        charge = 315
    elif daikin <= 1000000:
        charge = 840
    elif daikin <= 2000000:
        charge = 1680
    elif daikin <= 3000000:
        charge = 2520
    elif daikin <= 4000000:
        charge = 3360
    elif daikin <= 5000000:
        charge = 4200
    elif daikin <= 6000000:
        charge = 5040
    elif daikin <= 7000000:
        charge = 5880
    return (charge)


def makeDowDicts(file):
    dowOpens = {}
    dowCloses = {}
    dowHighs = {}
    dowLows = {}
    for dL in reversed(file.readlines()):
        dCells = patcomma.split(dL.strip())
        dowOpens[convertDateFormat4Dow(dCells[0])] = dCells[1]
        dowCloses[convertDateFormat4Dow(dCells[0])] = dCells[4]
        dowHighs[convertDateFormat4Dow(dCells[0])] = dCells[2]
        dowLows[convertDateFormat4Dow(dCells[0])] = dCells[3]
    # print dowDict
    return (dowOpens, dowCloses, dowHighs, dowLows)


if __name__ == "__main__":
    # ship(x, done=0):
    budget = 5000000
    b_ri = 0
    s_ri = 0
    profs4All = {}
    b_katsuhi = 0
    b_makehi = 0
    s_katsuhi = 0
    s_makehi = 0
    global allKatsu, allMake, allWake
    allKatsu = 0
    allMake = 0
    allWake = 0
    s_katsu = 0
    s_make = 0
    s_wake = 0
    tCount = 0
    board1SymListFile = open("/home/wailoktam/tradeData/tosho1.csv", "rb")
    symList = makeSymList(board1SymListFile)
    dowFile = open("/home/wailoktam/tradeData/HistoricalPrices.csv", "r")
    n225FileList = ["/home/wailoktam/tradeData/n225/n225-2000.csv", "/home/wailoktam/tradeData/n225/n225-2001.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2002.csv", "/home/wailoktam/tradeData/n225/n225-2003.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2004.csv", "/home/wailoktam/tradeData/n225/n225-2005.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2006.csv", "/home/wailoktam/tradeData/n225/n225-2007.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2008.csv", "/home/wailoktam/tradeData/n225/n225-2009.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2010.csv", "/home/wailoktam/tradeData/n225/n225-2011.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2012.csv", "/home/wailoktam/tradeData/n225/n225-2013.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2014.csv", "/home/wailoktam/tradeData/n225/n225-2015.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2016.csv"]

    dowOpens, dowCloses, dowHighs, dowLows = makeDowDicts(dowFile)
    n225Dict = n225DictFunct(n225FileList)
    dowFile.close()
    for currentday in date_generator(date(2000, 1, 5), date(2016, 12, 31), timedelta(days=1)):
        #        print currentday.isoformat()
        if n225Dict.has_key(currentday.isoformat()):
            yesterday225 = currentday - delta
            yesterday = currentday - delta
            while 1:
                if dowOpens.has_key(yesterday.isoformat()) and dowOpens.has_key(yesterday.isoformat()):
                    break
                else:
                    yesterday = yesterday - delta
            while 1:
                if n225Dict.has_key(yesterday225.isoformat()):
                    break
                else:
                    #                    print yesterday225
                    yesterday225 = yesterday225 - delta
            dayB4Yest = yesterday - delta
            while 1:
                if dowOpens.has_key(dayB4Yest.isoformat()):
                    break
                else:
                    dayB4Yest = dayB4Yest - delta
            dPrevHike = dowCloses[yesterday.isoformat()]
            dPrevYori = dowOpens[yesterday.isoformat()]
            dPrevHigh = dowHighs[yesterday.isoformat()]
            dPrevLow = dowLows[yesterday.isoformat()]
            dPrevPrevHike = dowCloses[dayB4Yest.isoformat()]
            profs4All = int_s_hike_b(currentday.isoformat(), list(max20ShortProfsLastYr.keys()), profs4All, yesterday225.isoformat(), dPrevYori,
                                      dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike, budget)

    totalProfAll = sum(profs4All.values())

    print("Sell Symbols:", totalProfAll, allKatsu, allMake, ((1.0 * allKatsu) + (0.5 * allWake)) / (allKatsu + allMake + allWake),
          totalProfAll / (allKatsu + allWake + allMake))

print len(bugSym)

#max20ShortProfs1stYr = dict(sorted(profs4All.iteritems(), key=operator.itemgetter(1), reverse=True)[:20])

#with open('max20ShortProfs1Yr.pickle', 'wb') as handle:
#    pickle.dump(max20ShortProfs1stYr, handle, protocol=pickle.HIGHEST_PROTOCOL)

#with open('bugSym.pickle', 'wb') as handle:
#    pickle.dump(bugSym, handle, protocol=pickle.HIGHEST_PROTOCOL)

#('Sell Symbols:', 244986781.1570462, 5638, 3508, 0.6075974944433219, 24751.139741063467)

