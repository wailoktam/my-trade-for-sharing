#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import codecs
import operator
import re
import os
import datetime
import time
import numpy
import scipy
import pickle
from datetime import timedelta, date
from decimal import Decimal
from scipy import stats, optimize
import collections

import math
from math import sqrt
from statistics import mean, stdev, pstdev


patcomma = re.compile(r",", re.DOTALL)
patcolon = re.compile(r":", re.DOTALL)
patdot = re.compile(r"\.", re.DOTALL)
patNonIsoDateN225 = re.compile(r"(\d\d\d\d)(\d\d)(\d\d)", re.DOTALL)
patNonIsoDateDow = re.compile(r"(\d\d)\/(\d\d)\/(\d\d)", re.DOTALL)
patLast2Digit = re.compile(r"\d\d(\d\d)", re.DOTALL)

#bugSym = {}


with open('bugSym.pickle', mode='rb') as handle:
    bugSym = pickle.load(handle)

ccounter = 0

debugFile = open("debug", "w")






delta = timedelta(days=1)





def push(someList, item):
    someList = numpy.roll(someList, 1)
#    print ("someList")
#    print (someList)
#    print ("someList0")
#   print (someList[0])
#    print ("item")
#    print item
    someList[0] = item
    return someList



def makeSymList(symFile):
    latestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2017/y171027.txt", "rb")
    earliestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2000/y000104.txt", "rb")
    symList = []
    latestList = []
    earliestList = []
    symLines = symFile.readlines()
    #    print symLines
    latestLines = latestPriceFile.readlines()
    earliestLines = earliestPriceFile.readlines()
    for l in latestLines:
        sym = l.split("\t")[0].strip()
        latestList.append(sym)
    # print latestList
    for e in earliestLines:
        sym = e.split("\t")[0].strip()
        #        print "sym in earliest"
        #        print sym
        earliestList.append(sym)
    # print earliestList
    for s in symLines:
        sym = s.strip()
        symList.append(sym)
    return (set(latestList) & set(earliestList))


def makeLotSizeDict(symList):
    lotsizefile = open("/home/wailoktam/tradeData/stock.csv", "r")
    lotsizedict = {}
    lotsizelines = lotsizefile.readlines()
    for lotsizeline in lotsizelines:
        lotsizes = patcomma.split(lotsizeline)
        sym = lotsizes[0].strip()
        if sym in symList:
            lotsizedict[sym] = lotsizes[6].strip()
    lotsizefile.close()
    return (lotsizedict)

def z(x, meanX, sdX):
    return (x-meanX)/sdX

def linest(pairs):
    x_seq = tuple(p[0] for p in pairs)
    y_seq = tuple(p[1] for p in pairs)
    meanX, sdX = mean(x_seq), stdev(x_seq)
    meanY, sdY = mean(y_seq), stdev(y_seq)
    z_x = (z(x, meanX, sdX) for x in x_seq)
    z_y = (z(y, meanY, sdY) for y in y_seq)
    r_xy = sum( zx*zy for zx, zy in zip(z_x, z_y)) /len(pairs)
    beta = r_xy * sdY/sdX
    alpha = meanY- beta*meanX
    return r_xy, alpha, beta


def tick_cal(price):
    if price < 2000:
        tick = 1
    elif price >= 2000 and price < 3000:
        tick = 5
    elif price >= 3000 and price < 30000:
        tick = 10
    elif price >= 30000 and price < 50000:
        tick = 50
    elif price >= 50000 and price < 100000:
        tick = 100
    elif price >= 100000 and price < 1000000:
        tick = 1000
    elif price >= 1000000 and price < 20000000:
        tick = 10000
    elif price >= 20000000 and price < 30000000:
        tick = 50000
    else:
        tick = 100000
    return (tick)


def date_generator(start_date, end_date, delta):
    d = start_date
    while d <= end_date:
        yield d
        d += delta


def neg_date_generator(start_date, end_date, delta):
    d = start_date
    while d >= end_date:
        yield d
        d -= delta


def convertDateFormat4N225(dateStr):
    matchDateStr = patNonIsoDateN225.search(dateStr)
    isoStr = matchDateStr.group(1) + "-" + matchDateStr.group(2) + "-" + matchDateStr.group(3)
    return (isoStr)


def convertDateFormat4Dow(dateStr):
    matchDateStr = patNonIsoDateDow.search(dateStr)
    isoStr = "20" + matchDateStr.group(3) + "-" + matchDateStr.group(1) + "-" + matchDateStr.group(2)
    return (isoStr)


def n225DictFunct(n225FileList):
    n225Dict = {}
    for fN in n225FileList:
        f = open(fN, "rb")
        n225Lines = f.readlines()
        for nL in n225Lines:
            n = patcomma.split(nL)
            dayStr = n[0].strip()
            n225Dict[convertDateFormat4N225(dayStr)] = n[4].strip()
    return (n225Dict)

def nxDictFunct(nxFile):
    nxDict = {}
    nxLines = nxFile.readlines()
    for nL in nxLines:
        n = patcomma.split(nL)
        dayStr = n[0].strip()
        nxDict[dayStr] = n[4].strip()
    return (nxDict)


def entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow, dowPrevPrevHike):
    prevDowPlusOrMinusSpec = float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow)) / 2
    prevDowChange = (float(dowPrevHike) - float(dowPrevPrevHike)) / float(dowPrevPrevHike)
    #    prevDowChange = (float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow))/2)/((float(dowPrevHigh) + float(dowPrevLow))/2)
    #    debugFile.write("prevDowPlusOrMinus\n")
    #    debugFile.write(str(prevDowPlusOrMinusSpec)+"\n")
    #    debugFile.write("dowPrevPrevHike*1.02\n")
    ##    debugFile.write(str(float(dowPrevPrevHike)*1.002)+"\n")
    #    debugFile.write("dowPrevHike in entry_cal\n")
    #    debugFile.write(str(float(dowPrevHike))+"\n")
    if prevDowPlusOrMinusSpec > 0 and float(dowPrevHike) > (float(dowPrevPrevHike) * 1.005):
        entryPrice = float(prevHike) * (1 + prevDowChange)
        if entryPrice < 2000: entryPrice = round(entryPrice)
        elif entryPrice >= 2000 and entryPrice < 3000:  entryPrice = round(entryPrice/5)*5
        elif entryPrice >= 3000 and entryPrice < 30000: entryPrice = round(entryPrice,-1)
        elif entryPrice >= 30000 and entryPrice < 50000: entryPrice = round(entryPrice/5,-1)*5
        elif entryPrice >= 50000 and entryPrice < 100000: entryPrice = round(entryPrice,-2)
        elif entryPrice >= 100000 and entryPrice < 1000000: entryPrice = round(entryPrice,-3)
        elif entryPrice >= 1000000 and entryPrice < 20000000: entryPrice = round(entryPrice,-4)
        elif entryPrice >= 20000000 and entryPrice < 30000000: entryPrice = round(entryPrice/5,-4)*5
        else: entryPrice = round(entryPrice,-5)
        entryPrice = entryPrice + tick_cal(entryPrice)
    else:
        entryPrice = 0
    return (entryPrice)


def exit_cal(entryPrice):
    exitPrice = entryPrice * 1.03
    if exitPrice < 2000:
        exitPrice = round(exitPrice)
    elif exitPrice >= 2000 and exitPrice < 3000:
        exitPrice = round(exitPrice / 5) * 5
    elif exitPrice >= 3000 and exitPrice < 30000:
        exitPrice = round(exitPrice, -1)
    elif exitPrice >= 30000 and exitPrice < 50000:
        exitPrice = round(exitPrice / 5, -1) * 5
    elif exitPrice >= 50000 and exitPrice < 100000:
        exitPrice = round(exitPrice, -2)
    elif exitPrice >= 100000 and exitPrice < 1000000:
        exitPrice = round(exitPrice, -3)
    elif exitPrice >= 1000000 and exitPrice < 20000000:
        exitPrice = round(exitPrice, -4)
    elif exitPrice >= 20000000 and exitPrice < 30000000:
        exitPrice = round(exitPrice / 5, -4) * 5
    else:
        exitPrice = round(exitPrice, -5)
    exitPrice = exitPrice + tick_cal(exitPrice)
#    exitPrice = 0
    return (exitPrice)



def sort(list, field):
    res = []  # always returns a list
    for x in list:
        i = 0
        for y in res:
            if x[field] <= y[field]: break  # list node goes here?
            i = i + 1
        res[i:i] = [x]  # insert in result slot
    return res



def makeSymPriceDict(symList, fullPath):
    symOpens = {}
    symCloses = {}
    symHighs = {}
    symLows = {}
    vol = {}
#    print fullPath
    file = open(fullPath, "rb")
    for l in file.readlines():
#        print l
        if l.split("\t")[0] in symList:
            symOpens[l.split("\t")[0]] = float(l.split("\t")[2])
            symCloses[l.split("\t")[0]] = float(l.split("\t")[5])
            symHighs[l.split("\t")[0]] = float(l.split("\t")[3])
            symLows[l.split("\t")[0]] = float(l.split("\t")[4])
            try:
                vol[l.split("\t")[0]] = float(l.split("\t")[6])
            except ValueError:
                pass
#            print l.split("\t")[0]
#            print symOpens[l.split("\t")[0]]
#            print symCloses[l.split("\t")[0]]
#            print symHighs[l.split("\t")[0]]
    file.close()
    return (symOpens, symCloses, symHighs, symLows,vol)




def int_s_lc(isoDate, symList, profs, prevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget, bugSym, dbgOpt):
    daikin = 0
    mcounter = 0


    profOpt = 0


    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath
    fullPath = folderPath + "/" + filePath

    try:
        symOpens, symCloses, symHighs, _, vols = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        _, prevSymCloses, _, _, prevVols = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0


    entry = 0
    #    print "stockPriceFound"
    #    print stockPriceFileFound
    if stockPriceFileFound == 1:
        #       for s in ["4704"]:
#        print "symList"
#        print symList
        for s in symList:

            #            try:
            #            if s in max20ShortProfs1stYr:
            if s not in bugSym:
#                print "prevSymCloses"
#                print prevSymCloses
#                try:
#                    pass
#                except KeyError:

#                    print isoDate
#                    print symOpens
                prevHike = prevSymCloses[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]
                vol = vols[s]
                lotSize = float(lotSizeDict[s])


#                gap = (prevHike -yori) * 1.0


#                if s in unitChange.keys():
#                    print "hit1"
#                    print unitChange[s][0]
#                    print dateutil.parser.parse(isoDate)
#                    if unitChange[s][0] > dateutil.parser.parse(isoDate):
#                        print"hit2"
#                        lotSize =  unitChange[s][1]


                prevVol = prevVols[s]
                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

#                print "symbol"
#                print s
#                print "yori"
#                print yori
#                print "hike"
#                print hike
#                print "high"
#                print high
#                print "prevHike"
#                print prevHike
#                print "minEntry"
#                print minEntryPrice
#                print "entry"
#                print entry


#                debugFile.write("dowPrevyori\n")
#                debugFile.write(str(dowPrevYori) + "\n")
#                debugFile.write("dowPrevhike\n")
#                debugFile.write(str(dowPrevHike) + "\n")
#                debugFile.write("dowPrevhigh\n")
#                debugFile.write(str(dowPrevHigh) + "\n")
#                debugFile.write("dowPrevLow\n")
#                debugFile.write(str(dowPrevLow) + "\n")
#                debugFile.write("dowPrevprevHike\n")
#                debugFile.write(str(dowPrevPrevHike) + "\n")

                if entry == 1:

                    maxExitPrice = exit_cal(entryPrice)

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not profs.get(s, 0) == 0:
                        if entryPrice * float(lotSize) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 0
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf = (entryPrice - tejimai) * lotSize
                                profs[s] = profs[s] + sameDayProf

                            else:
                                profOpt = 1
                                sameDayProf = (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = profs[s] + sameDayProf

                        else:
                            profOpt = 2
                            sameDayProf =  ((entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget))
                            profs[s] = profs[s] + sameDayProf
                    else:
                        if entryPrice * lotSize <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 3
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf =  (entryPrice - tejimai) * lotSize
                                profs[s] = sameDayProf
                            else:
                                profOpt = 4
                                sameDayProf =  (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = sameDayProf

                        else:
                            profOpt = 5
                            sameDayProf = (entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget)
                            profs[s] = sameDayProf

#                        lastclose = (string.atoi(prev[len(prev) - 1][4])) * 1000
#                        yori = (string.atoi(temp[0][1])) * 1000

#                        lc = round((yori * 1.03), -3)
#                        rcounter = 0
#                        while 1:







                    # daikin = daikin + entry
#                    if entryPrice - tejimai >  0 and not (budget / hike * 10) > (vol * 1000):
#                        katsu = katsu + 1
#                    elif entryPrice - tejimai < 0 and not (budget / hike * 10) > (vol * 1000):
#                        make = make + 1
#                    else:
#                        if not (budget / hike * 10) > (vol * 1000):
#                            wake = wake + 1
                    if dbgOpt == "debug":
#                        if sameDayProf > 100000 or sameDayProf < -100000:
                            debugFile.write(isoDate + "\n")
                            debugFile.write("symbol\n")
                            debugFile.write(str(s) + "\n")
                            debugFile.write("yori\n")
                            debugFile.write(str(yori) + "\n")
                            debugFile.write("hike\n")
                            debugFile.write(str(hike) + "\n")
                            debugFile.write("high\n")
                            debugFile.write(str(high) + "\n")
                            debugFile.write("prevHike\n")
                            debugFile.write(str(prevHike) + "\n")
                            debugFile.write("minEntry\n")
                            debugFile.write(str(minEntryPrice) + "\n")
                            debugFile.write("entry\n")
                            debugFile.write(str(entry) + "\n")
                            debugFile.write("lotsize\n")
                            debugFile.write(str(lotSize) + "\n")
                            debugFile.write("vol\n")
                            debugFile.write(str(vol*1000) + "\n")
                            debugFile.write("entryPice\n")
                            debugFile.write(str(entryPrice) + "\n")
                            debugFile.write("tejimai\n")
                            debugFile.write(str(tejimai) + "\n")
                            debugFile.write("sameDayProf\n")
                            debugFile.write(str(sameDayProf)+ "\n")
                            debugFile.write("profOpt\n")
                            debugFile.write(str(profOpt) + "\n")


                    #            except KeyError:
                    #                bugSym[s] = ""
                    #                pass
    return (shortProfs)


def int_s_lc_mo(isoDate, symList, profs, betas, nxDict, prevIsoDate, prevPrevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget, dbgOpt):
    daikin = 0
    mcounter = 0


    profOpt = 0


    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    fullPath = folderPath + "/" + filePath
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath
    prevPrevLast2Digit = patLast2Digit.search(prevPrevIsoDate.split("-")[0]).group(1)
    prevPrevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevPrevIsoDate.split("-")[0]
    prevPrevFilePath = "y" + prevPrevLast2Digit + prevPrevIsoDate.split("-")[1] + prevPrevIsoDate.split("-")[2] + ".txt"
    prevPrevFullPath = prevPrevFolderPath + "/" + prevPrevFilePath




    try:
        symOpens, symCloses, symHighs, _, vols = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        prevSymOpens, prevSymCloses, _, _, prevVols = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0

    try:
        _, prevPrevSymCloses, _, _, _ = makeSymPriceDict(symList, prevPrevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0


    if stockPriceFileFound == 1:

        for s in symList:

            #            try:
            #            if s in max20ShortProfs1stYr:
            if s not in bugSym:

                prevNxHike = nxDict[prevIsoDate]
                nxHike = nxDict[isoDate]
                prevHike = prevSymCloses[s]

                prevPrevHike = prevPrevSymCloses[s]
                prevYori = prevSymOpens[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]
                vol = vols[s]
                lotSize = float(lotSizeDict[s])





                prevVol = prevVols[s]
                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0 and not prevHike <= prevYori *0.98:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0  and not prevHike <= prevYori *0.98:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0



                if entry == 1:

                    maxExitPrice = exit_cal(entryPrice)

                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not profs.get(s, 0) == 0:
                        if entryPrice * float(lotSize) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 0
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf = (entryPrice - tejimai) * lotSize
                                profs[s] = profs[s] + sameDayProf

                            else:
                                profOpt = 1
                                sameDayProf = (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = profs[s] + sameDayProf

                        else:
                            profOpt = 2
                            sameDayProf =  ((entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget))
                            profs[s] = profs[s] + sameDayProf
                    else:
                        if entryPrice * lotSize <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                profOpt = 3
                                #not count low volume
                                sameDayProf = 0
#                                sameDayProf =  (entryPrice - tejimai) * lotSize
                                profs[s] = sameDayProf
                            else:
                                profOpt = 4
                                sameDayProf =  (entryPrice - tejimai) * lotSize * (
                                    round(budget / (entryPrice * lotSize) - 0.5))
                                profs[s] = sameDayProf

                        else:
                            profOpt = 5
                            sameDayProf = (entryPrice - tejimai) * lotSize /(entryPrice*lotSize/budget)
                            profs[s] = sameDayProf


                    if dbgOpt == "debug":
#                        if sameDayProf > 100000 or sameDayProf < -100000:
                            debugFile.write(isoDate + "\n")
                            debugFile.write("symbol\n")
                            debugFile.write(str(s) + "\n")
                            debugFile.write("yori\n")
                            debugFile.write(str(yori) + "\n")
                            debugFile.write("hike\n")
                            debugFile.write(str(hike) + "\n")
                            debugFile.write("high\n")
                            debugFile.write(str(high) + "\n")
                            debugFile.write("prevHike\n")
                            debugFile.write(str(prevHike) + "\n")
                            debugFile.write("minEntry\n")
                            debugFile.write(str(minEntryPrice) + "\n")
                            debugFile.write("entry\n")
                            debugFile.write(str(entry) + "\n")
                            debugFile.write("lotsize\n")
                            debugFile.write(str(lotSize) + "\n")
                            debugFile.write("vol\n")
                            debugFile.write(str(vol*1000) + "\n")
                            debugFile.write("entryPice\n")
                            debugFile.write(str(entryPrice) + "\n")
                            debugFile.write("tejimai\n")
                            debugFile.write(str(tejimai) + "\n")
                            debugFile.write("sameDayProf\n")
                            debugFile.write(str(sameDayProf)+ "\n")
                            debugFile.write("profOpt\n")
                            debugFile.write(str(profOpt) + "\n")


                if s in betas:
                    if len(betas[s]) < 100:
                        betas[s] = numpy.append(betas[s],
                        numpy.array([((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1)]))

                    else:
                        betas[s] = push(betas[s], ((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1))
                else:
                    betas[s] = numpy.array([((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1)], dtype=object)
                    #            except KeyError:
                    #                bugSym[s] = ""
                    #                pass
    return (shortProfs, betas)



def int_s_hike_b(isoDate, symList, shortProfs, prevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike, budget, bugSym, dbgOpt):
    global s_katsu, s_make, s_wake

    daikin = 0
    mcounter = 0

    lotSizeDict = makeLotSizeDict(symList)
    pathNameDate = "".join(isoDate.split("-"))
    prevPathNameDate = "".join(prevIsoDate.split("-"))
    last2Digit = patLast2Digit.search(isoDate.split("-")[0]).group(1)
    folderPath = "/home/wailoktam/tradeData/stockPrice/" + isoDate.split("-")[0]
    filePath = "y" + last2Digit + isoDate.split("-")[1] + isoDate.split("-")[2] + ".txt"
    prevLast2Digit = patLast2Digit.search(prevIsoDate.split("-")[0]).group(1)
    prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + prevIsoDate.split("-")[0]
    prevFilePath = "y" + prevLast2Digit + prevIsoDate.split("-")[1] + prevIsoDate.split("-")[2] + ".txt"
    prevFullPath = prevFolderPath + "/" + prevFilePath
    fullPath = folderPath + "/" + filePath

    try:
        symOpens, symCloses, symHighs, _, vols = makeSymPriceDict(symList, fullPath)
        stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    try:
        _, prevSymCloses, _, _, prevVols = makeSymPriceDict(symList, prevFullPath)
        if stockPriceFileFound == 1: stockPriceFileFound = 1
    except IOError:
        stockPriceFileFound = 0
    entry = 0
    if stockPriceFileFound == 1:
        for s in symList:
            if s not in bugSym:
#            try:
                prevHike = prevSymCloses[s]
                yori = symOpens[s]
                hike = symCloses[s]
                high = symHighs[s]
                vol = vols[s]

                prevVol = prevVols[s]

                minEntryPrice = entry_cal(prevHike, yori, hike, high, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike)
                #        maxExitPrice = exit_cal(prevHike, yori,hike, high, dowPrevYori,dowPrevHike,dowPrevHigh,dowPrevLow)
                maxExitPrice = 0
                if yori >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = yori
                    entry = 1


                elif high >= minEntryPrice and not minEntryPrice == 0:
                    entryPrice = minEntryPrice
                    entry = 1
                else:

                    entry = 0

                if entry == 1:
                    if high >= maxExitPrice and not maxExitPrice == 0:
                        tejimai = maxExitPrice
                    else:
                        tejimai = hike

                    if not shortProfs.get(s, 0) == 0:
                        if entryPrice * float(lotSizeDict[s]) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                shortProfs[s] = shortProfs[s] + (entryPrice - tejimai) * float(lotSizeDict[s])
                            else:
                                shortProfs[s] = shortProfs[s] + (entryPrice - tejimai) * float(lotSizeDict[s]) * (
                                round(budget / (entryPrice * float(lotSizeDict[s])) - 0.5))
                        else:
                            shortProfs[s] = shortProfs[s] + ((entryPrice - tejimai) * float(lotSizeDict[s])/(entryPrice*float(lotSizeDict[s])/budget))
                    else:
                        if entryPrice * float(lotSizeDict[s]) <= budget:
                            if (budget / hike * 10) > (vol * 1000):
                                shortProfs[s] = (entryPrice - tejimai) * float(lotSizeDict[s])
                            else:
                                shortProfs[s] = (entryPrice - tejimai) * float(lotSizeDict[s]) * (
                                round(budget / (entryPrice * float(lotSizeDict[s])) - 0.5))

                        else:
                            shortProfs[s] = (entryPrice - tejimai) * float(lotSizeDict[s])/(entryPrice*float(lotSizeDict[s])/budget)





                    # daikin = daikin + entry
                    if entryPrice - tejimai > 0:
                        s_katsu = s_katsu + 1
                    elif entryPrice - tejimai < 0:
                        s_make = s_make + 1
                    else:
                        s_wake = s_wake + 1
#            except KeyError:
#                if not s in bugSym:
#                    bugSym[s] = ""

                    #      except IndexError:
                    #            pass
    return (shortProfs)





def old_int_b_hike_s(day1, meigaralist, b_ri, prev, commod, dow, formula_list, vhsd_list, y):
    global b_katsu, b_make, b_wake
    daikin = 0
    mcounter = 0
    #    subdict=lotsizedict()
    for meigara in meigaralist:
        temp = []
        for shou in range(1, 5):
            if shou <= 3:
                shou_cp = shou
            else:
                shou_cp = 9
            for bu in range(1, 5):
                shoubu = str(shou_cp) + str(bu)
                day2 = "".join(day1.split("-"))
                candlefilename = "../candles/" + day2 + "/" + meigara + "." + shoubu + ".csv"
                try:
                    candlefile = open(candlefilename, "r")
                except IOError:
                    pass
                else:
                    if os.path.getsize(candlefilename) > 0:
                        rows = candlefile.readlines()
                        for row in rows:
                            cells = patcomma.split(row)
                            temp.append(tuple(cells))
                    candlefile.close()
        temp = sort(temp, 0)
        try:
            prevtemp = []
            for shou in range(1, 5):
                if shou <= 3:
                    shou_cp = shou
                else:
                    shou_cp = 9
                for bu in range(1, 5):
                    shoubu = str(shou_cp) + str(bu)
                    day3 = "".join(prev.split("-"))
                    prevcandlefilename = "../candles/" + day3 + "/" + meigara + "." + shoubu + ".csv"
                    try:
                        prevcandlefile = open(prevcandlefilename, "r")
                    except IOError:
                        pass
                    else:
                        if os.path.getsize(prevcandlefilename) > 0:
                            rows = prevcandlefile.readlines()
                            for row in rows:
                                cells = patcomma.split(row)
                                prevtemp.append(tuple(cells))
                        prevcandlefile.close()
            prevtemp = sort(prevtemp, 0)
            prevhike = (string.atoi(prevtemp[len(prevtemp) - 1][4]))
            yori = (string.atoi(temp[0][1])) * (string.atoi(subdict[meigara]))
            hike = (string.atoi(temp[len(temp) - 1][4])) * (string.atoi(subdict[meigara]))

            max_entry = entry_cal(prevhike, commod, dow, formula_list[mcounter], vhsd_list[mcounter], y, "l") * (
            string.atoi(subdict[meigara]))
            min_exit = exit_cal(prevhike, commod, dow, formula_list[mcounter], vhsd_list[mcounter], y, "l") * (
            string.atoi(subdict[meigara]))

            if yori <= max_entry:
                entry = yori
                action = "entry"
                rcounter = 0
                while 1:
                    if (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) <= min_exit) and (
                            string.atoi(temp[rcounter][2]) * (string.atoi(subdict[meigara])) > min_exit):
                        tejimai = min_exit
                        break
                    elif (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) > min_exit):
                        tejimai = string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara]))
                        break
                    else:
                        if rcounter == len(temp) - 1:
                            tejimai = string.atoi(temp[len(temp) - 1][4]) * (string.atoi(subdict[meigara]))
                            break
                    rcounter = rcounter + 1
            else:
                rcounter = 0
                action = "noentry"
                while 1:
                    if (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) < max_entry):
                        entry = string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara]))
                        action = "entry"
                        entry_pt = rcounter
                        break
                    elif (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) >= max_entry) and (
                            string.atoi(temp[rcounter][2]) * (string.atoi(subdict[meigara])) < max_entry):
                        entry = max_entry
                        action = "entry"
                        entry_pt = rcounter
                        break
                    if rcounter == len(temp) - 1:
                        break
                    rcounter = rcounter + 1
                rcounter = 0
                if action == "entry":
                    while 1:
                        if (string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara])) <= min_exit) and (
                                string.atoi(temp[rcounter][2]) * (
                            string.atoi(subdict[meigara])) > min_exit) and rcounter > entry_pt:
                            tejimai = min_exit
                            break
                        elif (string.atoi(temp[rcounter][1]) * (
                        string.atoi(subdict[meigara])) > min_exit) and rcounter > entry_pt:
                            tejimai = string.atoi(temp[rcounter][1]) * (string.atoi(subdict[meigara]))
                            break
                        else:
                            if rcounter == len(temp) - 1:
                                tejimai = string.atoi(temp[len(temp) - 1][4]) * (string.atoi(subdict[meigara]))
                                break
                        rcounter = rcounter + 1

            if action != "noentry":
                b_ri = b_ri + (tejimai - entry)
                daikin = daikin + entry
                if tejimai - entry > 0:
                    b_katsu = b_katsu + 1
                elif tejimai - entry < 0:
                    b_make = b_make + 1
                else:
                    b_wake = b_wake + 1
            # print(day1,meigara,entry,tejimai,entry-tejimai,min_entry)
            candlefile.close()
            prevcandlefile.close()
        except IndexError:
            pass
        mcounter = mcounter + 1
    return ((b_ri, daikin))


def tesuuryou(daikin):
    if daikin == 0:
        charge = 0
    elif daikin <= 500000:
        charge = 315
    elif daikin <= 1000000:
        charge = 840
    elif daikin <= 2000000:
        charge = 1680
    elif daikin <= 3000000:
        charge = 2520
    elif daikin <= 4000000:
        charge = 3360
    elif daikin <= 5000000:
        charge = 4200
    elif daikin <= 6000000:
        charge = 5040
    elif daikin <= 7000000:
        charge = 5880
    return (charge)


def makeDowDicts(file):
    dowOpens = {}
    dowCloses = {}
    dowHighs = {}
    dowLows = {}
    for dL in reversed(file.readlines()):
        dCells = patcomma.split(dL.strip())
        dowOpens[convertDateFormat4Dow(dCells[0])] = dCells[1]
        dowCloses[convertDateFormat4Dow(dCells[0])] = dCells[4]
        dowHighs[convertDateFormat4Dow(dCells[0])] = dCells[2]
        dowLows[convertDateFormat4Dow(dCells[0])] = dCells[3]
    # print dowDict
    return (dowOpens, dowCloses, dowHighs, dowLows)


if __name__ == "__main__":
    # ship(x, done=0):
    budget = 1000000
    b_ri = 0
    s_ri = 0
    betaDict = {}
    shortProfs = {}
    b_katsuhi = 0
    b_makehi = 0
    s_katsuhi = 0
    s_makehi = 0
    global b_katsu, b_make, b_wake, s_katsu, s_make, s_wake
    b_katsu = 0
    b_make = 0
    b_wake = 0
    s_katsu = 0
    s_make = 0
    s_wake = 0
    tCount = 0
#    board1SymListFile = open("/home/wailoktam/tradeData/tosho1.csv", "rb")
    allBoardSymListFile = open("/home/wailoktam/tradeData/stock.csv", "rb")
    symList = makeSymList(allBoardSymListFile)
    nxFile = open("/home/wailoktam/tradeData/nx/nx.csv", "r")
    dowFile = open("/home/wailoktam/tradeData/HistoricalPrices.csv", "r")
    n225FileList = ["/home/wailoktam/tradeData/n225/n225-2000.csv", "/home/wailoktam/tradeData/n225/n225-2001.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2002.csv", "/home/wailoktam/tradeData/n225/n225-2003.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2004.csv", "/home/wailoktam/tradeData/n225/n225-2005.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2006.csv", "/home/wailoktam/tradeData/n225/n225-2007.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2008.csv", "/home/wailoktam/tradeData/n225/n225-2009.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2010.csv", "/home/wailoktam/tradeData/n225/n225-2011.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2012.csv", "/home/wailoktam/tradeData/n225/n225-2013.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2014.csv", "/home/wailoktam/tradeData/n225/n225-2015.csv", \
                    "/home/wailoktam/tradeData/n225/n225-2016.csv"]
#taken from http://quotes.wsj.com/index/DJIA/historical-prices
    dowOpens, dowCloses, dowHighs, dowLows = makeDowDicts(dowFile)
    n225Dict = n225DictFunct(n225FileList)
    nxDict = nxDictFunct(nxFile)
    dowFile.close()
    for currentday in date_generator(date(2000, 1, 6), date(2000, 12, 31), timedelta(days=1)):
        #        print currentday.isoformat()
        if n225Dict.has_key(currentday.isoformat()):
            yesterday225 = currentday - delta
            yesterday = currentday - delta
            while 1:
                if dowOpens.has_key(yesterday.isoformat()) and dowOpens.has_key(yesterday.isoformat()):
                    break
                else:
                    yesterday = yesterday - delta
            while 1:
                if n225Dict.has_key(yesterday225.isoformat()):
                    break
                else:
                    #                    print yesterday225
                    yesterday225 = yesterday225 - delta
            dayB4Yest = yesterday - delta
            dayB4Yest225 = yesterday225 - delta
            while 1:
                if dowOpens.has_key(dayB4Yest.isoformat()):
                    break
                else:
                    dayB4Yest = dayB4Yest - delta

            while 1:
                if n225Dict.has_key(dayB4Yest225.isoformat()):
                    break
                else:
                    dayB4Yest225 = dayB4Yest225 - delta

            dPrevHike = dowCloses[yesterday.isoformat()]
            dPrevYori = dowOpens[yesterday.isoformat()]
            dPrevHigh = dowHighs[yesterday.isoformat()]
            dPrevLow = dowLows[yesterday.isoformat()]
            dPrevPrevHike = dowCloses[dayB4Yest.isoformat()]
            shortProfs, betaDict = int_s_lc_mo(currentday.isoformat(), symList, shortProfs, betaDict, nxDict, yesterday225.isoformat(),  dayB4Yest225.isoformat(), dPrevYori,
                                      dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike, budget, "debug")




#print bugSym

#max20ShortProfs1stYr = dict(sorted(shortProfs.iteritems(), key=operator.itemgetter(1), reverse=True)[:20])
max10ShortProfs1stYr = dict(sorted(shortProfs.iteritems(), key=operator.itemgetter(1), reverse=True)[:10])

#with open('max20ShortProfs1Yr.pickle', 'wb') as mHandle:
#    pickle.dump(max20ShortProfs1stYr, mHandle, protocol=pickle.HIGHEST_PROTOCOL)


debugFile.close()

with open('betaDict.pickle', 'wb') as mHandle:
    pickle.dump(betaDict, mHandle, protocol=pickle.HIGHEST_PROTOCOL)


with open('max10ShortProfs1Yr.pickle', 'wb') as mHandle:
    pickle.dump(max10ShortProfs1stYr, mHandle, protocol=pickle.HIGHEST_PROTOCOL)

with open('profs4All.pickle', 'wb') as rHandle:
    pickle.dump(shortProfs, rHandle, protocol=pickle.HIGHEST_PROTOCOL)



#with open('bugSym.pickle', 'wb') as handle:
#    pickle.dump(bugSym, handle, protocol=pickle.HIGHEST_PROTOCOL)