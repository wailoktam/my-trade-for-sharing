#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import codecs
import copy
import re
import os
import datetime
import time
import numpy
import scipy
import pickle
import operator
from datetime import timedelta, date, datetime
from decimal import Decimal
from scipy import stats, optimize
import dateutil.parser
import matplotlib
import collections
from statistics import mean, stdev, pstdev
matplotlib.use('Agg')
from matplotlib import pyplot, dates

import math
from math import sqrt

patcomma = re.compile(r",", re.DOTALL)
patcolon = re.compile(r":", re.DOTALL)
patdot = re.compile(r"\.", re.DOTALL)
patNonIsoDateN225 = re.compile(r"(\d\d\d\d)(\d\d)(\d\d)", re.DOTALL)
patNonIsoDateDow = re.compile(r"(\d\d)\/(\d\d)\/(\d\d)", re.DOTALL)
patLast2Digit = re.compile(r"\d\d(\d\d)", re.DOTALL)

with open('bugSym.pickle', mode='rb') as handle:
    bugSym = pickle.load(handle)

ccounter = 0

debugFile = open("debug", "w")

delta = timedelta(days=1)


def z(x, meanX, sdX):
    return (x-meanX)/sdX

def linest(pairs):
    x_seq = tuple(p[0] for p in pairs)
    y_seq = tuple(p[1] for p in pairs)
    meanX, sdX = mean(x_seq), stdev(x_seq)
    meanY, sdY = mean(y_seq), stdev(y_seq)
    z_x = (z(x, meanX, sdX) for x in x_seq)
    z_y = (z(y, meanY, sdY) for y in y_seq)
    r_xy = sum( zx*zy for zx, zy in zip(z_x, z_y)) /len(pairs)
    beta = r_xy * sdY/sdX
    alpha = meanY- beta*meanX
    return r_xy, alpha, beta

def polyfit(x, y, degree):
#    results = {}

    coeffs = numpy.polyfit(x, y, degree)
     # Polynomial Coefficients
#    results['polynomial'] = coeffs.tolist()
    # r-squared
    p = numpy.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = numpy.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = numpy.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = numpy.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    return ssreg / sstot


def push(someList, item):
    someList = numpy.roll(someList, 1)
#    print ("someList")
#    print (someList)
#    print ("someList0")
#   print (someList[0])
#    print ("item")
#    print item
    someList[0] = item
    return someList


def makeSymList(symFile):
    latestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2017/y171027.txt", "rb")
    earliestPriceFile = open("/home/wailoktam/tradeData/stockPrice/2000/y000104.txt", "rb")
    symList = []
    latestList = []
    earliestList = []
    symLines = symFile.readlines()
    #    print symLines
    latestLines = latestPriceFile.readlines()
    earliestLines = earliestPriceFile.readlines()
    for l in latestLines:
        sym = l.split("\t")[0].strip()
        latestList.append(sym)
    # print latestList
    for e in earliestLines:
        sym = e.split("\t")[0].strip()
        #        print "sym in earliest"
        #        print sym
        earliestList.append(sym)
    # print earliestList
    for s in symLines:
        sym = s.strip()
        symList.append(sym)
    return (set(latestList) & set(earliestList))


def makeLotSizeDict(symList):
    lotsizefile = open("/home/wailoktam/tradeData/stock.csv", "r")
    lotsizedict = {}
    lotsizelines = lotsizefile.readlines()
    for lotsizeline in lotsizelines:
        lotsizes = patcomma.split(lotsizeline)
        sym = lotsizes[0].strip()
        if sym in symList:
            lotsizedict[sym] = lotsizes[6].strip()
    lotsizefile.close()
    return (lotsizedict)


def tick_cal(price):
    if price < 2000:
        tick = 1
    elif price >= 2000 and price < 3000:
        tick = 5
    elif price >= 3000 and price < 30000:
        tick = 10
    elif price >= 30000 and price < 50000:
        tick = 50
    elif price >= 50000 and price < 100000:
        tick = 100
    elif price >= 100000 and price < 1000000:
        tick = 1000
    elif price >= 1000000 and price < 20000000:
        tick = 10000
    elif price >= 20000000 and price < 30000000:
        tick = 50000
    else:
        tick = 100000
    return (tick)


def date_generator(start_date, end_date, delta):
    d = start_date
    while d <= end_date:
        yield d
        d += delta


def neg_date_generator(start_date, end_date, delta):
    d = start_date
    while d >= end_date:
        yield d
        d -= delta


def convertDateFormat4N225(dateStr):
    matchDateStr = patNonIsoDateN225.search(dateStr)
    isoStr = matchDateStr.group(1) + "-" + matchDateStr.group(2) + "-" + matchDateStr.group(3)
    return (isoStr)


def convertDateFormat4Dow(dateStr):
    matchDateStr = patNonIsoDateDow.search(dateStr)
    isoStr = "20" + matchDateStr.group(3) + "-" + matchDateStr.group(1) + "-" + matchDateStr.group(2)
    return (isoStr)


def nxDictFunct(nxFile):
    nxDict = {}
    nxLines = nxFile.readlines()
    for nL in nxLines:
        n = patcomma.split(nL)
        dayStr = n[0].strip()
        nxDict[dayStr] = n[4].strip()
    return (nxDict)

def reviseNxDict(day, close, nxDict):
    nxDict[day] = close
    return (nxDict)




def entry_cal(prevHike, dowPrevHike, dowPrevHigh, dowPrevLow, dowPrevPrevHike, beta):
    prevDowPlusOrMinusSpec = float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow)) / 2
    prevDowChange = (float(dowPrevHike) - float(dowPrevPrevHike)) / float(dowPrevPrevHike)
    #    prevDowChange = (float(dowPrevHike) - (float(dowPrevHigh) + float(dowPrevLow))/2)/((float(dowPrevHigh) + float(dowPrevLow))/2)
    #    debugFile.write("prevDowPlusOrMinus\n")
    #    debugFile.write(str(prevDowPlusOrMinusSpec)+"\n")
    #    debugFile.write("dowPrevPrevHike*1.02\n")
    ##    debugFile.write(str(float(dowPrevPrevHike)*1.002)+"\n")
    #    debugFile.write("dowPrevHike in entry_cal\n")
    #    debugFile.write(str(float(dowPrevHike))+"\n")
    if prevDowPlusOrMinusSpec > 0 and float(dowPrevHike) > (float(dowPrevPrevHike) * 1.005):
        entryPrice = float(prevHike) * (1 + prevDowChange*beta)
        if entryPrice < 2000: entryPrice = round(entryPrice)
        elif entryPrice >= 2000 and entryPrice < 3000:  entryPrice = round(entryPrice/5)*5
        elif entryPrice >= 3000 and entryPrice < 30000: entryPrice = round(entryPrice,-1)
        elif entryPrice >= 30000 and entryPrice < 50000: entryPrice = round(entryPrice/5,-1)*5
        elif entryPrice >= 50000 and entryPrice < 100000: entryPrice = round(entryPrice,-2)
        elif entryPrice >= 100000 and entryPrice < 1000000: entryPrice = round(entryPrice,-3)
        elif entryPrice >= 1000000 and entryPrice < 20000000: entryPrice = round(entryPrice,-4)
        elif entryPrice >= 20000000 and entryPrice < 30000000: entryPrice = round(entryPrice/5,-4)*5
        else: entryPrice = round(entryPrice,-5)
        entryPrice = entryPrice + tick_cal(entryPrice)
    else:
        entryPrice = 0
    return (entryPrice)


def exit_cal(entryPrice, beta):
    exitPrice = entryPrice * (1+(0.03*beta))
    if exitPrice < 2000:
        exitPrice = round(exitPrice)
    elif exitPrice >= 2000 and exitPrice < 3000:
        exitPrice = round(exitPrice / 5) * 5
    elif exitPrice >= 3000 and exitPrice < 30000:
        exitPrice = round(exitPrice, -1)
    elif exitPrice >= 30000 and exitPrice < 50000:
        exitPrice = round(exitPrice / 5, -1) * 5
    elif exitPrice >= 50000 and exitPrice < 100000:
        exitPrice = round(exitPrice, -2)
    elif exitPrice >= 100000 and exitPrice < 1000000:
        exitPrice = round(exitPrice, -3)
    elif exitPrice >= 1000000 and exitPrice < 20000000:
        exitPrice = round(exitPrice, -4)
    elif exitPrice >= 20000000 and exitPrice < 30000000:
        exitPrice = round(exitPrice / 5, -4) * 5
    else:
        exitPrice = round(exitPrice, -5)
    exitPrice = exitPrice + tick_cal(exitPrice)
#    exitPrice = 0
    return (exitPrice)


def sort(list, field):
    res = []  # always returns a list
    for x in list:
        i = 0
        for y in res:
            if x[field] <= y[field]: break  # list node goes here?
            i = i + 1
        res[i:i] = [x]  # insert in result slot
    return res



def reviseSymPriceDict(day, open, high, low, close, symOpens, symCloses, symHighs, symLows):
    symOpens[day] = float(open)
    symCloses[day] = float(close)
    symHighs[day] = float(high)
    symLows[day] = float(low)
    return (symOpens, symCloses, symHighs, symLows)

def makeSymPriceDict(symList, symOpens, symCloses, symHighs, symLows, fullPath):
    file = open(fullPath, "rb")
    for l in file.readlines():
        if l.split("\t")[0] in symList:
            symOpens[l.split("\t")[0]] = float(l.split("\t")[2])
            symCloses[l.split("\t")[0]] = float(l.split("\t")[5])
            symHighs[l.split("\t")[0]] = float(l.split("\t")[3])
            symLows[l.split("\t")[0]] = float(l.split("\t")[4])
            try:
                vol[l.split("\t")[0]] = float(l.split("\t")[6])
            except ValueError:
                pass
    file.close()
    return (symOpens, symCloses, symHighs, symLows)


def int_s_lc_mo(isoDate, symList, betas, nxDict, prevIsoDate, dowPrevYori, dowPrevHike, dowPrevHigh, dowPrevLow,
                 dowPrevPrevHike,  prevSymOpens, prevSymCloses, betaOpt):
    daikin = 0
    mcounter = 0


    profOpt = 0


    lotSizeDict = makeLotSizeDict(symList)
    entry = 0
    for s in symList:
        prevNxHike = nxDict[prevIsoDate]
#        nxHike = nxDict[isoDate]
        prevHike = prevSymCloses[s]
        prevYori = prevSymOpens[s]
        lotSize = float(lotSizeDict[s])
        r2 = polyfit([i[0] for i in betas[s]], [i[1] for i in betas[s]], 1)

        if r2 > 0.5:
            beta = linest(betas[s])[2]
        else:
            beta = 1



        minEntryPrice = entry_cal(prevHike, dowPrevHike, dowPrevHigh, dowPrevLow,
                                          dowPrevPrevHike, beta)
        if not prevHike <= prevYori * 0.98:
            print ("entry price ("+s+"):"+ str(minEntryPrice))
            print ("beta("+s+"):" + str(beta))


        if betaOpt == "betaCalc":
            if s in betas:
                if len(betas[s]) < 100:
                    betas[s] = numpy.append(betas[s], numpy.array([((float(nxHike) / float(prevNxHike)) - 1,(hike / prevHike) - 1)]))

                else:
                    betas[s] = push(betas[s], ((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1))
            else:
                betas[s] = numpy.array([((float(nxHike) / float(prevNxHike)) - 1, (hike / prevHike) - 1)],
                                                   dtype=object)



                            #            except KeyError:
                    #                bugSym[s] = ""
                    #                pass
    return (betas)













def tesuuryou(daikin):
    if daikin == 0:
        charge = 0
    elif daikin <= 500000:
        charge = 315
    elif daikin <= 1000000:
        charge = 840
    elif daikin <= 2000000:
        charge = 1680
    elif daikin <= 3000000:
        charge = 2520
    elif daikin <= 4000000:
        charge = 3360
    elif daikin <= 5000000:
        charge = 4200
    elif daikin <= 6000000:
        charge = 5040
    elif daikin <= 7000000:
        charge = 5880
    return (charge)


def makeDowDicts(file):
    dowOpens = {}
    dowCloses = {}
    dowHighs = {}
    dowLows = {}
    for dL in reversed(file.readlines()):
        dCells = patcomma.split(dL.strip())
        dowOpens[convertDateFormat4Dow(dCells[0])] = dCells[1]
        dowHighs[convertDateFormat4Dow(dCells[0])] = dCells[2]
        dowLows[convertDateFormat4Dow(dCells[0])] = dCells[3]
        dowCloses[convertDateFormat4Dow(dCells[0])] = dCells[4]
    # print dowDict
    return (dowOpens, dowCloses, dowHighs, dowLows)

def reviseDowDicts(day, open, high, low, close, dowOpens, dowHighs, dowLows, dowCloses):

    dowOpens[day] = open
    dowCloses[day] = close
    dowHighs[day]= high
    dowLows[day] = low
    # print dowDict
    return (dowOpens, dowCloses, dowHighs, dowLows)




def difference_dict(Dict_A, Dict_B):
    output_dict = {}
    for key in Dict_A.keys():
        if key in Dict_B.keys():
            output_dict[key] = Dict_A[key] - Dict_B[key]
    return output_dict









def mixHistNNew(noHistYear, noHistMonth, noHistDay, thisYear, thisMonth, today, max10ShortProfsLastYr, betaDict, nxDict, dowOpens, dowCloses, dowHighs, dowLows, action):

    prevSymOpens = {"2211":2548, "9438":655,"6507":455, "4716":9550, "6762":8410, "6310":2473, "5210":200, "5911":2325, "9715":2587, "5809":818}
    prevSymCloses = {"2211":2572, "9438":681,"6507":460,"4716":9550, "6762":8670, "6310":2515, "5210":199, "5911":2412, "9715":2633, "5809":829}

#    prevSymOpens = {}
#    prevSymCloses = {}

    year = noHistYear
    while year <= thisYear:



        for currentday in date_generator(date(noHistYear, noHistMonth, noHistDay), date(thisYear, thisMonth, today), timedelta(days=1)):

            #        print currentday.isoformat()
            if nxDict.has_key(currentday.isoformat()):
                yesterdayNx = currentday - delta
                yesterday = currentday - delta

#                print yesterday
#                print yesterday225
                while 1:
                    if dowOpens.has_key(yesterday.isoformat()) and dowOpens.has_key(yesterday.isoformat()):
                        break
                    else:
                        #                   print yesterday
                        yesterday = yesterday - delta
                while 1:
                    if nxDict.has_key(yesterdayNx.isoformat()):
                        break
                    else:
                        #                    print yesterday225
                        yesterdayNx = yesterdayNx - delta

                dayB4Yest = yesterday - delta

                while 1:
                    if dowOpens.has_key(dayB4Yest.isoformat()):
                        break
                    else:
                        #                    print yesterday225
                        dayB4Yest = dayB4Yest - delta




                dPrevHike = dowCloses[yesterday.isoformat()]
                dPrevYori = dowOpens[yesterday.isoformat()]
                dPrevHigh = dowHighs[yesterday.isoformat()]

                dPrevLow = dowLows[yesterday.isoformat()]
                dPrevPrevHike = dowCloses[dayB4Yest.isoformat()]

                prevLast2Digit = patLast2Digit.search((yesterdayNx.isoformat()).split("-")[0]).group(1)
                prevFolderPath = "/home/wailoktam/tradeData/stockPrice/" + (yesterdayNx.isoformat()).split("-")[0]
                prevFilePath = "y" + prevLast2Digit +  (yesterdayNx.isoformat()).split("-")[1] +  (yesterdayNx.isoformat()).split("-")[2] + ".txt"
                prevFullPath = prevFolderPath + "/" + prevFilePath

                if prevSymOpens == {}:
                    prevSymOpens, prevSymCloses, _, _ = makeSymPriceDict(symList, prevFullPath)

                print  yesterdayNx.isoformat(), dPrevYori, dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike

                betaDict = int_s_lc_mo(currentday.isoformat(), list(max10ShortProfsLastYr.keys()), betaDict, nxDict,
                                                                     yesterdayNx.isoformat(), dPrevYori,
                                                                     dPrevHike, dPrevHigh, dPrevLow, dPrevPrevHike,
                                                                    prevSymCloses, prevSymOpens,
                                                                    "BetaCalc")




        year += 1






    return

def main():
    budget = 1000000

    noHistYear = 2017
    noHistMonth = 10
    noHistDay = 30
    today = 1
    thisMonth= 11
    thisYear=2017


    dowFile = open("/home/wailoktam/tradeData/HistoricalPrices.csv", "r")
    dowOpens, dowCloses, dowHighs, dowLows = makeDowDicts(dowFile)
    dowFile.close()
    dowOpens, dowCloses, dowHighs, dowLows  = reviseDowDicts("2017-10-31", "23369.22", "23406.35", "23334.39", "23377.24", dowOpens, dowCloses, dowHighs, dowLows)


    with open('betaDict.pickle', 'rb') as mHandle:
        betaDict = pickle.load(mHandle)

    with open('max10ShortProfs1Yr.pickle', 'rb') as mHandle:
        max10ShortProfsLastYr = pickle.load(mHandle)

    nxFile = open("/home/wailoktam/tradeData/nx/nx.csv", "r")


    action = "entry"
#   action = "exit"

    nxDict = nxDictFunct(nxFile)

    nxDict =  reviseNxDict("2017-10-31", "22011.61", nxDict)

    mixHistNNew(noHistYear, noHistMonth, noHistDay, thisYear, thisMonth, today,  max10ShortProfsLastYr, betaDict, nxDict, dowOpens, dowCloses, dowHighs, dowLows, action)







if __name__ == "__main__":
    main()

